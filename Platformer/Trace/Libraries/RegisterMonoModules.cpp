#include "RegisterMonoModules.h"
#include <stdio.h>

#if defined(TARGET_IPHONE_SIMULATOR) && TARGET_IPHONE_SIMULATOR
    #define DECL_USER_FUNC(f) void f() __attribute__((weak_import))
    #define REGISTER_USER_FUNC(f)\
        do {\
        if(f != NULL)\
            mono_dl_register_symbol(#f, (void*)f);\
        else\
            ::printf_console("Symbol '%s' not found. Maybe missing implementation for Simulator?\n", #f);\
        }while(0)
#else
    #define DECL_USER_FUNC(f) void f() 
    #if !defined(__arm64__)
    #define REGISTER_USER_FUNC(f) mono_dl_register_symbol(#f, (void*)&f)
    #else
        #define REGISTER_USER_FUNC(f)
    #endif
#endif
extern "C"
{
    typedef void* gpointer;
    typedef int gboolean;
    const char*         UnityIPhoneRuntimeVersion = "5.5.0f3";
    void                mono_dl_register_symbol (const char* name, void *addr);
#if !defined(__arm64__)
    extern int          mono_ficall_flag;
#endif
    void                mono_aot_register_module(gpointer *aot_info);
#if __ORBIS__ || SN_TARGET_PSP2
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT
#endif
#if !(TARGET_IPHONE_SIMULATOR)
    extern gboolean     mono_aot_only;
    extern gpointer*    mono_aot_module_Assembly_CSharp_info; // Assembly-CSharp.dll
    extern gpointer*    mono_aot_module_System_Core_info; // System.Core.dll
    extern gpointer*    mono_aot_module_System_info; // System.dll
    extern gpointer*    mono_aot_module_UnityEngine_Advertisements_iOS_info; // UnityEngine.Advertisements.iOS.dll
    extern gpointer*    mono_aot_module_UnityEngine_Networking_info; // UnityEngine.Networking.dll
    extern gpointer*    mono_aot_module_UnityEngine_UI_info; // UnityEngine.UI.dll
    extern gpointer*    mono_aot_module_UnityEngine_info; // UnityEngine.dll
    extern gpointer*    mono_aot_module_mscorlib_info; // mscorlib.dll
#endif // !(TARGET_IPHONE_SIMULATOR)
}
DLL_EXPORT void RegisterMonoModules()
{
#if !(TARGET_IPHONE_SIMULATOR) && !defined(__arm64__)
    mono_aot_only = true;
    mono_ficall_flag = false;
    mono_aot_register_module(mono_aot_module_Assembly_CSharp_info);
    mono_aot_register_module(mono_aot_module_System_Core_info);
    mono_aot_register_module(mono_aot_module_System_info);
    mono_aot_register_module(mono_aot_module_UnityEngine_Advertisements_iOS_info);
    mono_aot_register_module(mono_aot_module_UnityEngine_Networking_info);
    mono_aot_register_module(mono_aot_module_UnityEngine_UI_info);
    mono_aot_register_module(mono_aot_module_UnityEngine_info);
    mono_aot_register_module(mono_aot_module_mscorlib_info);
#endif // !(TARGET_IPHONE_SIMULATOR) && !defined(__arm64__)

}

void RegisterAllStrippedInternalCalls ()
{
	void Register_UnityEngine_AndroidJNI_AttachCurrentThread ();
	Register_UnityEngine_AndroidJNI_AttachCurrentThread ();
	void Register_UnityEngine_AndroidJNI_DetachCurrentThread ();
	Register_UnityEngine_AndroidJNI_DetachCurrentThread ();
	void Register_UnityEngine_AndroidJNI_GetVersion ();
	Register_UnityEngine_AndroidJNI_GetVersion ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FindClass ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FindClass ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedMethod ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedMethod ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedField ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedField ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToReflectedMethod ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToReflectedMethod ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToReflectedField ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToReflectedField ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetSuperclass ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetSuperclass ();
	void Register_UnityEngine_AndroidJNI_IsAssignableFrom ();
	Register_UnityEngine_AndroidJNI_IsAssignableFrom ();
	void Register_UnityEngine_AndroidJNI_Throw ();
	Register_UnityEngine_AndroidJNI_Throw ();
	void Register_UnityEngine_AndroidJNI_ThrowNew ();
	Register_UnityEngine_AndroidJNI_ThrowNew ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ExceptionOccurred ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ExceptionOccurred ();
	void Register_UnityEngine_AndroidJNI_ExceptionDescribe ();
	Register_UnityEngine_AndroidJNI_ExceptionDescribe ();
	void Register_UnityEngine_AndroidJNI_ExceptionClear ();
	Register_UnityEngine_AndroidJNI_ExceptionClear ();
	void Register_UnityEngine_AndroidJNI_FatalError ();
	Register_UnityEngine_AndroidJNI_FatalError ();
	void Register_UnityEngine_AndroidJNI_PushLocalFrame ();
	Register_UnityEngine_AndroidJNI_PushLocalFrame ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_PopLocalFrame ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_PopLocalFrame ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewGlobalRef ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewGlobalRef ();
	void Register_UnityEngine_AndroidJNI_DeleteGlobalRef ();
	Register_UnityEngine_AndroidJNI_DeleteGlobalRef ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewLocalRef ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewLocalRef ();
	void Register_UnityEngine_AndroidJNI_DeleteLocalRef ();
	Register_UnityEngine_AndroidJNI_DeleteLocalRef ();
	void Register_UnityEngine_AndroidJNI_IsSameObject ();
	Register_UnityEngine_AndroidJNI_IsSameObject ();
	void Register_UnityEngine_AndroidJNI_EnsureLocalCapacity ();
	Register_UnityEngine_AndroidJNI_EnsureLocalCapacity ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_AllocObject ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_AllocObject ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObject ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObject ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectClass ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectClass ();
	void Register_UnityEngine_AndroidJNI_IsInstanceOf ();
	Register_UnityEngine_AndroidJNI_IsInstanceOf ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetMethodID ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetMethodID ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetFieldID ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetFieldID ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticMethodID ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticMethodID ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticFieldID ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticFieldID ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewStringUTF ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewStringUTF ();
	void Register_UnityEngine_AndroidJNI_GetStringUTFLength ();
	Register_UnityEngine_AndroidJNI_GetStringUTFLength ();
	void Register_UnityEngine_AndroidJNI_GetStringUTFChars ();
	Register_UnityEngine_AndroidJNI_GetStringUTFChars ();
	void Register_UnityEngine_AndroidJNI_CallStringMethod ();
	Register_UnityEngine_AndroidJNI_CallStringMethod ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallObjectMethod ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallObjectMethod ();
	void Register_UnityEngine_AndroidJNI_CallIntMethod ();
	Register_UnityEngine_AndroidJNI_CallIntMethod ();
	void Register_UnityEngine_AndroidJNI_CallBooleanMethod ();
	Register_UnityEngine_AndroidJNI_CallBooleanMethod ();
	void Register_UnityEngine_AndroidJNI_CallShortMethod ();
	Register_UnityEngine_AndroidJNI_CallShortMethod ();
	void Register_UnityEngine_AndroidJNI_CallByteMethod ();
	Register_UnityEngine_AndroidJNI_CallByteMethod ();
	void Register_UnityEngine_AndroidJNI_CallCharMethod ();
	Register_UnityEngine_AndroidJNI_CallCharMethod ();
	void Register_UnityEngine_AndroidJNI_CallFloatMethod ();
	Register_UnityEngine_AndroidJNI_CallFloatMethod ();
	void Register_UnityEngine_AndroidJNI_CallDoubleMethod ();
	Register_UnityEngine_AndroidJNI_CallDoubleMethod ();
	void Register_UnityEngine_AndroidJNI_CallLongMethod ();
	Register_UnityEngine_AndroidJNI_CallLongMethod ();
	void Register_UnityEngine_AndroidJNI_CallVoidMethod ();
	Register_UnityEngine_AndroidJNI_CallVoidMethod ();
	void Register_UnityEngine_AndroidJNI_GetStringField ();
	Register_UnityEngine_AndroidJNI_GetStringField ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectField ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectField ();
	void Register_UnityEngine_AndroidJNI_GetBooleanField ();
	Register_UnityEngine_AndroidJNI_GetBooleanField ();
	void Register_UnityEngine_AndroidJNI_GetByteField ();
	Register_UnityEngine_AndroidJNI_GetByteField ();
	void Register_UnityEngine_AndroidJNI_GetCharField ();
	Register_UnityEngine_AndroidJNI_GetCharField ();
	void Register_UnityEngine_AndroidJNI_GetShortField ();
	Register_UnityEngine_AndroidJNI_GetShortField ();
	void Register_UnityEngine_AndroidJNI_GetIntField ();
	Register_UnityEngine_AndroidJNI_GetIntField ();
	void Register_UnityEngine_AndroidJNI_GetLongField ();
	Register_UnityEngine_AndroidJNI_GetLongField ();
	void Register_UnityEngine_AndroidJNI_GetFloatField ();
	Register_UnityEngine_AndroidJNI_GetFloatField ();
	void Register_UnityEngine_AndroidJNI_GetDoubleField ();
	Register_UnityEngine_AndroidJNI_GetDoubleField ();
	void Register_UnityEngine_AndroidJNI_SetStringField ();
	Register_UnityEngine_AndroidJNI_SetStringField ();
	void Register_UnityEngine_AndroidJNI_SetObjectField ();
	Register_UnityEngine_AndroidJNI_SetObjectField ();
	void Register_UnityEngine_AndroidJNI_SetBooleanField ();
	Register_UnityEngine_AndroidJNI_SetBooleanField ();
	void Register_UnityEngine_AndroidJNI_SetByteField ();
	Register_UnityEngine_AndroidJNI_SetByteField ();
	void Register_UnityEngine_AndroidJNI_SetCharField ();
	Register_UnityEngine_AndroidJNI_SetCharField ();
	void Register_UnityEngine_AndroidJNI_SetShortField ();
	Register_UnityEngine_AndroidJNI_SetShortField ();
	void Register_UnityEngine_AndroidJNI_SetIntField ();
	Register_UnityEngine_AndroidJNI_SetIntField ();
	void Register_UnityEngine_AndroidJNI_SetLongField ();
	Register_UnityEngine_AndroidJNI_SetLongField ();
	void Register_UnityEngine_AndroidJNI_SetFloatField ();
	Register_UnityEngine_AndroidJNI_SetFloatField ();
	void Register_UnityEngine_AndroidJNI_SetDoubleField ();
	Register_UnityEngine_AndroidJNI_SetDoubleField ();
	void Register_UnityEngine_AndroidJNI_CallStaticStringMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticStringMethod ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallStaticObjectMethod ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallStaticObjectMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticIntMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticIntMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticBooleanMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticBooleanMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticShortMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticShortMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticByteMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticByteMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticCharMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticCharMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticFloatMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticFloatMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticDoubleMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticDoubleMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticLongMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticLongMethod ();
	void Register_UnityEngine_AndroidJNI_CallStaticVoidMethod ();
	Register_UnityEngine_AndroidJNI_CallStaticVoidMethod ();
	void Register_UnityEngine_AndroidJNI_GetStaticStringField ();
	Register_UnityEngine_AndroidJNI_GetStaticStringField ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticObjectField ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticObjectField ();
	void Register_UnityEngine_AndroidJNI_GetStaticBooleanField ();
	Register_UnityEngine_AndroidJNI_GetStaticBooleanField ();
	void Register_UnityEngine_AndroidJNI_GetStaticByteField ();
	Register_UnityEngine_AndroidJNI_GetStaticByteField ();
	void Register_UnityEngine_AndroidJNI_GetStaticCharField ();
	Register_UnityEngine_AndroidJNI_GetStaticCharField ();
	void Register_UnityEngine_AndroidJNI_GetStaticShortField ();
	Register_UnityEngine_AndroidJNI_GetStaticShortField ();
	void Register_UnityEngine_AndroidJNI_GetStaticIntField ();
	Register_UnityEngine_AndroidJNI_GetStaticIntField ();
	void Register_UnityEngine_AndroidJNI_GetStaticLongField ();
	Register_UnityEngine_AndroidJNI_GetStaticLongField ();
	void Register_UnityEngine_AndroidJNI_GetStaticFloatField ();
	Register_UnityEngine_AndroidJNI_GetStaticFloatField ();
	void Register_UnityEngine_AndroidJNI_GetStaticDoubleField ();
	Register_UnityEngine_AndroidJNI_GetStaticDoubleField ();
	void Register_UnityEngine_AndroidJNI_SetStaticStringField ();
	Register_UnityEngine_AndroidJNI_SetStaticStringField ();
	void Register_UnityEngine_AndroidJNI_SetStaticObjectField ();
	Register_UnityEngine_AndroidJNI_SetStaticObjectField ();
	void Register_UnityEngine_AndroidJNI_SetStaticBooleanField ();
	Register_UnityEngine_AndroidJNI_SetStaticBooleanField ();
	void Register_UnityEngine_AndroidJNI_SetStaticByteField ();
	Register_UnityEngine_AndroidJNI_SetStaticByteField ();
	void Register_UnityEngine_AndroidJNI_SetStaticCharField ();
	Register_UnityEngine_AndroidJNI_SetStaticCharField ();
	void Register_UnityEngine_AndroidJNI_SetStaticShortField ();
	Register_UnityEngine_AndroidJNI_SetStaticShortField ();
	void Register_UnityEngine_AndroidJNI_SetStaticIntField ();
	Register_UnityEngine_AndroidJNI_SetStaticIntField ();
	void Register_UnityEngine_AndroidJNI_SetStaticLongField ();
	Register_UnityEngine_AndroidJNI_SetStaticLongField ();
	void Register_UnityEngine_AndroidJNI_SetStaticFloatField ();
	Register_UnityEngine_AndroidJNI_SetStaticFloatField ();
	void Register_UnityEngine_AndroidJNI_SetStaticDoubleField ();
	Register_UnityEngine_AndroidJNI_SetStaticDoubleField ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToBooleanArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToBooleanArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToByteArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToByteArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToCharArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToCharArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToShortArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToShortArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToIntArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToIntArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToLongArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToLongArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToFloatArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToFloatArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToDoubleArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToDoubleArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToObjectArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToObjectArray ();
	void Register_UnityEngine_AndroidJNI_FromBooleanArray ();
	Register_UnityEngine_AndroidJNI_FromBooleanArray ();
	void Register_UnityEngine_AndroidJNI_FromByteArray ();
	Register_UnityEngine_AndroidJNI_FromByteArray ();
	void Register_UnityEngine_AndroidJNI_FromCharArray ();
	Register_UnityEngine_AndroidJNI_FromCharArray ();
	void Register_UnityEngine_AndroidJNI_FromShortArray ();
	Register_UnityEngine_AndroidJNI_FromShortArray ();
	void Register_UnityEngine_AndroidJNI_FromIntArray ();
	Register_UnityEngine_AndroidJNI_FromIntArray ();
	void Register_UnityEngine_AndroidJNI_FromLongArray ();
	Register_UnityEngine_AndroidJNI_FromLongArray ();
	void Register_UnityEngine_AndroidJNI_FromFloatArray ();
	Register_UnityEngine_AndroidJNI_FromFloatArray ();
	void Register_UnityEngine_AndroidJNI_FromDoubleArray ();
	Register_UnityEngine_AndroidJNI_FromDoubleArray ();
	void Register_UnityEngine_AndroidJNI_FromObjectArray ();
	Register_UnityEngine_AndroidJNI_FromObjectArray ();
	void Register_UnityEngine_AndroidJNI_GetArrayLength ();
	Register_UnityEngine_AndroidJNI_GetArrayLength ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewBooleanArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewBooleanArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewByteArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewByteArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewCharArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewCharArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewShortArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewShortArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewIntArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewIntArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewLongArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewLongArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewFloatArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewFloatArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewDoubleArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewDoubleArray ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObjectArray ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObjectArray ();
	void Register_UnityEngine_AndroidJNI_GetBooleanArrayElement ();
	Register_UnityEngine_AndroidJNI_GetBooleanArrayElement ();
	void Register_UnityEngine_AndroidJNI_GetByteArrayElement ();
	Register_UnityEngine_AndroidJNI_GetByteArrayElement ();
	void Register_UnityEngine_AndroidJNI_GetCharArrayElement ();
	Register_UnityEngine_AndroidJNI_GetCharArrayElement ();
	void Register_UnityEngine_AndroidJNI_GetShortArrayElement ();
	Register_UnityEngine_AndroidJNI_GetShortArrayElement ();
	void Register_UnityEngine_AndroidJNI_GetIntArrayElement ();
	Register_UnityEngine_AndroidJNI_GetIntArrayElement ();
	void Register_UnityEngine_AndroidJNI_GetLongArrayElement ();
	Register_UnityEngine_AndroidJNI_GetLongArrayElement ();
	void Register_UnityEngine_AndroidJNI_GetFloatArrayElement ();
	Register_UnityEngine_AndroidJNI_GetFloatArrayElement ();
	void Register_UnityEngine_AndroidJNI_GetDoubleArrayElement ();
	Register_UnityEngine_AndroidJNI_GetDoubleArrayElement ();
	void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectArrayElement ();
	Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetBooleanArrayElement ();
	Register_UnityEngine_AndroidJNI_SetBooleanArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetByteArrayElement ();
	Register_UnityEngine_AndroidJNI_SetByteArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetCharArrayElement ();
	Register_UnityEngine_AndroidJNI_SetCharArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetShortArrayElement ();
	Register_UnityEngine_AndroidJNI_SetShortArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetIntArrayElement ();
	Register_UnityEngine_AndroidJNI_SetIntArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetLongArrayElement ();
	Register_UnityEngine_AndroidJNI_SetLongArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetFloatArrayElement ();
	Register_UnityEngine_AndroidJNI_SetFloatArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetDoubleArrayElement ();
	Register_UnityEngine_AndroidJNI_SetDoubleArrayElement ();
	void Register_UnityEngine_AndroidJNI_SetObjectArrayElement ();
	Register_UnityEngine_AndroidJNI_SetObjectArrayElement ();
	void Register_UnityEngine_AndroidJNIHelper_get_debug ();
	Register_UnityEngine_AndroidJNIHelper_get_debug ();
	void Register_UnityEngine_AndroidJNIHelper_set_debug ();
	Register_UnityEngine_AndroidJNIHelper_set_debug ();
	void Register_UnityEngine_AndroidJNIHelper_INTERNAL_CALL_CreateJavaProxy ();
	Register_UnityEngine_AndroidJNIHelper_INTERNAL_CALL_CreateJavaProxy ();
	void Register_UnityEngine_Animation_get_clip ();
	Register_UnityEngine_Animation_get_clip ();
	void Register_UnityEngine_Animation_set_clip ();
	Register_UnityEngine_Animation_set_clip ();
	void Register_UnityEngine_Animation_get_playAutomatically ();
	Register_UnityEngine_Animation_get_playAutomatically ();
	void Register_UnityEngine_Animation_set_playAutomatically ();
	Register_UnityEngine_Animation_set_playAutomatically ();
	void Register_UnityEngine_Animation_get_wrapMode ();
	Register_UnityEngine_Animation_get_wrapMode ();
	void Register_UnityEngine_Animation_set_wrapMode ();
	Register_UnityEngine_Animation_set_wrapMode ();
	void Register_UnityEngine_Animation_INTERNAL_CALL_Stop ();
	Register_UnityEngine_Animation_INTERNAL_CALL_Stop ();
	void Register_UnityEngine_Animation_Internal_StopByName ();
	Register_UnityEngine_Animation_Internal_StopByName ();
	void Register_UnityEngine_Animation_Internal_RewindByName ();
	Register_UnityEngine_Animation_Internal_RewindByName ();
	void Register_UnityEngine_Animation_INTERNAL_CALL_Rewind ();
	Register_UnityEngine_Animation_INTERNAL_CALL_Rewind ();
	void Register_UnityEngine_Animation_INTERNAL_CALL_Sample ();
	Register_UnityEngine_Animation_INTERNAL_CALL_Sample ();
	void Register_UnityEngine_Animation_get_isPlaying ();
	Register_UnityEngine_Animation_get_isPlaying ();
	void Register_UnityEngine_Animation_IsPlaying ();
	Register_UnityEngine_Animation_IsPlaying ();
	void Register_UnityEngine_Animation_Play ();
	Register_UnityEngine_Animation_Play ();
	void Register_UnityEngine_Animation_CrossFade ();
	Register_UnityEngine_Animation_CrossFade ();
	void Register_UnityEngine_Animation_Blend ();
	Register_UnityEngine_Animation_Blend ();
	void Register_UnityEngine_Animation_CrossFadeQueued ();
	Register_UnityEngine_Animation_CrossFadeQueued ();
	void Register_UnityEngine_Animation_PlayQueued ();
	Register_UnityEngine_Animation_PlayQueued ();
	void Register_UnityEngine_Animation_AddClip ();
	Register_UnityEngine_Animation_AddClip ();
	void Register_UnityEngine_Animation_RemoveClip ();
	Register_UnityEngine_Animation_RemoveClip ();
	void Register_UnityEngine_Animation_GetClipCount ();
	Register_UnityEngine_Animation_GetClipCount ();
	void Register_UnityEngine_Animation_RemoveClip2 ();
	Register_UnityEngine_Animation_RemoveClip2 ();
	void Register_UnityEngine_Animation_PlayDefaultAnimation ();
	Register_UnityEngine_Animation_PlayDefaultAnimation ();
	void Register_UnityEngine_Animation_INTERNAL_CALL_SyncLayer ();
	Register_UnityEngine_Animation_INTERNAL_CALL_SyncLayer ();
	void Register_UnityEngine_Animation_GetState ();
	Register_UnityEngine_Animation_GetState ();
	void Register_UnityEngine_Animation_GetStateAtIndex ();
	Register_UnityEngine_Animation_GetStateAtIndex ();
	void Register_UnityEngine_Animation_GetStateCount ();
	Register_UnityEngine_Animation_GetStateCount ();
	void Register_UnityEngine_Animation_get_animatePhysics ();
	Register_UnityEngine_Animation_get_animatePhysics ();
	void Register_UnityEngine_Animation_set_animatePhysics ();
	Register_UnityEngine_Animation_set_animatePhysics ();
	void Register_UnityEngine_Animation_get_animateOnlyIfVisible ();
	Register_UnityEngine_Animation_get_animateOnlyIfVisible ();
	void Register_UnityEngine_Animation_set_animateOnlyIfVisible ();
	Register_UnityEngine_Animation_set_animateOnlyIfVisible ();
	void Register_UnityEngine_Animation_get_cullingType ();
	Register_UnityEngine_Animation_get_cullingType ();
	void Register_UnityEngine_Animation_set_cullingType ();
	Register_UnityEngine_Animation_set_cullingType ();
	void Register_UnityEngine_Animation_INTERNAL_get_localBounds ();
	Register_UnityEngine_Animation_INTERNAL_get_localBounds ();
	void Register_UnityEngine_Animation_INTERNAL_set_localBounds ();
	Register_UnityEngine_Animation_INTERNAL_set_localBounds ();
	void Register_UnityEngine_AnimationClip_SampleAnimation ();
	Register_UnityEngine_AnimationClip_SampleAnimation ();
	void Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip ();
	Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip ();
	void Register_UnityEngine_AnimationClip_get_length ();
	Register_UnityEngine_AnimationClip_get_length ();
	void Register_UnityEngine_AnimationClip_get_startTime ();
	Register_UnityEngine_AnimationClip_get_startTime ();
	void Register_UnityEngine_AnimationClip_get_stopTime ();
	Register_UnityEngine_AnimationClip_get_stopTime ();
	void Register_UnityEngine_AnimationClip_get_frameRate ();
	Register_UnityEngine_AnimationClip_get_frameRate ();
	void Register_UnityEngine_AnimationClip_set_frameRate ();
	Register_UnityEngine_AnimationClip_set_frameRate ();
	void Register_UnityEngine_AnimationClip_SetCurve ();
	Register_UnityEngine_AnimationClip_SetCurve ();
	void Register_UnityEngine_AnimationClip_INTERNAL_CALL_EnsureQuaternionContinuity ();
	Register_UnityEngine_AnimationClip_INTERNAL_CALL_EnsureQuaternionContinuity ();
	void Register_UnityEngine_AnimationClip_INTERNAL_CALL_ClearCurves ();
	Register_UnityEngine_AnimationClip_INTERNAL_CALL_ClearCurves ();
	void Register_UnityEngine_AnimationClip_get_wrapMode ();
	Register_UnityEngine_AnimationClip_get_wrapMode ();
	void Register_UnityEngine_AnimationClip_set_wrapMode ();
	Register_UnityEngine_AnimationClip_set_wrapMode ();
	void Register_UnityEngine_AnimationClip_INTERNAL_get_localBounds ();
	Register_UnityEngine_AnimationClip_INTERNAL_get_localBounds ();
	void Register_UnityEngine_AnimationClip_INTERNAL_set_localBounds ();
	Register_UnityEngine_AnimationClip_INTERNAL_set_localBounds ();
	void Register_UnityEngine_AnimationClip_get_legacy ();
	Register_UnityEngine_AnimationClip_get_legacy ();
	void Register_UnityEngine_AnimationClip_set_legacy ();
	Register_UnityEngine_AnimationClip_set_legacy ();
	void Register_UnityEngine_AnimationClip_get_humanMotion ();
	Register_UnityEngine_AnimationClip_get_humanMotion ();
	void Register_UnityEngine_AnimationClip_AddEventInternal ();
	Register_UnityEngine_AnimationClip_AddEventInternal ();
	void Register_UnityEngine_AnimationClip_SetEventsInternal ();
	Register_UnityEngine_AnimationClip_SetEventsInternal ();
	void Register_UnityEngine_AnimationClip_GetEventsInternal ();
	Register_UnityEngine_AnimationClip_GetEventsInternal ();
	void Register_UnityEngine_AnimationCurve_Cleanup ();
	Register_UnityEngine_AnimationCurve_Cleanup ();
	void Register_UnityEngine_AnimationCurve_Evaluate ();
	Register_UnityEngine_AnimationCurve_Evaluate ();
	void Register_UnityEngine_AnimationCurve_AddKey ();
	Register_UnityEngine_AnimationCurve_AddKey ();
	void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_AddKey_Internal ();
	Register_UnityEngine_AnimationCurve_INTERNAL_CALL_AddKey_Internal ();
	void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_MoveKey ();
	Register_UnityEngine_AnimationCurve_INTERNAL_CALL_MoveKey ();
	void Register_UnityEngine_AnimationCurve_RemoveKey ();
	Register_UnityEngine_AnimationCurve_RemoveKey ();
	void Register_UnityEngine_AnimationCurve_get_length ();
	Register_UnityEngine_AnimationCurve_get_length ();
	void Register_UnityEngine_AnimationCurve_SetKeys ();
	Register_UnityEngine_AnimationCurve_SetKeys ();
	void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_GetKey_Internal ();
	Register_UnityEngine_AnimationCurve_INTERNAL_CALL_GetKey_Internal ();
	void Register_UnityEngine_AnimationCurve_GetKeys ();
	Register_UnityEngine_AnimationCurve_GetKeys ();
	void Register_UnityEngine_AnimationCurve_SmoothTangents ();
	Register_UnityEngine_AnimationCurve_SmoothTangents ();
	void Register_UnityEngine_AnimationCurve_get_preWrapMode ();
	Register_UnityEngine_AnimationCurve_get_preWrapMode ();
	void Register_UnityEngine_AnimationCurve_set_preWrapMode ();
	Register_UnityEngine_AnimationCurve_set_preWrapMode ();
	void Register_UnityEngine_AnimationCurve_get_postWrapMode ();
	Register_UnityEngine_AnimationCurve_get_postWrapMode ();
	void Register_UnityEngine_AnimationCurve_set_postWrapMode ();
	Register_UnityEngine_AnimationCurve_set_postWrapMode ();
	void Register_UnityEngine_AnimationCurve_Init ();
	Register_UnityEngine_AnimationCurve_Init ();
	void Register_UnityEngine_AnimationState_get_clip ();
	Register_UnityEngine_AnimationState_get_clip ();
	void Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo ();
	Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo ();
	void Register_UnityEngine_Animator_GetNextAnimatorStateInfo ();
	Register_UnityEngine_Animator_GetNextAnimatorStateInfo ();
	void Register_UnityEngine_Animator_GetAnimatorTransitionInfo ();
	Register_UnityEngine_Animator_GetAnimatorTransitionInfo ();
	void Register_UnityEngine_Animator_IsInTransition ();
	Register_UnityEngine_Animator_IsInTransition ();
	void Register_UnityEngine_Animator_get_parameters ();
	Register_UnityEngine_Animator_get_parameters ();
	void Register_UnityEngine_Animator_Play ();
	Register_UnityEngine_Animator_Play ();
	void Register_UnityEngine_Animator_get_runtimeAnimatorController ();
	Register_UnityEngine_Animator_get_runtimeAnimatorController ();
	void Register_UnityEngine_Animator_StringToHash ();
	Register_UnityEngine_Animator_StringToHash ();
	void Register_UnityEngine_Animator_SetFloatString ();
	Register_UnityEngine_Animator_SetFloatString ();
	void Register_UnityEngine_Animator_SetFloatID ();
	Register_UnityEngine_Animator_SetFloatID ();
	void Register_UnityEngine_Animator_GetFloatID ();
	Register_UnityEngine_Animator_GetFloatID ();
	void Register_UnityEngine_Animator_SetBoolString ();
	Register_UnityEngine_Animator_SetBoolString ();
	void Register_UnityEngine_Animator_SetBoolID ();
	Register_UnityEngine_Animator_SetBoolID ();
	void Register_UnityEngine_Animator_GetBoolString ();
	Register_UnityEngine_Animator_GetBoolString ();
	void Register_UnityEngine_Animator_GetBoolID ();
	Register_UnityEngine_Animator_GetBoolID ();
	void Register_UnityEngine_Animator_SetIntegerID ();
	Register_UnityEngine_Animator_SetIntegerID ();
	void Register_UnityEngine_Animator_GetIntegerID ();
	Register_UnityEngine_Animator_GetIntegerID ();
	void Register_UnityEngine_Animator_SetTriggerString ();
	Register_UnityEngine_Animator_SetTriggerString ();
	void Register_UnityEngine_Animator_SetTriggerID ();
	Register_UnityEngine_Animator_SetTriggerID ();
	void Register_UnityEngine_Animator_ResetTriggerString ();
	Register_UnityEngine_Animator_ResetTriggerString ();
	void Register_UnityEngine_AnimatorClipInfo_ClipInstanceToScriptingObject ();
	Register_UnityEngine_AnimatorClipInfo_ClipInstanceToScriptingObject ();
	void Register_UnityEngine_Application_Quit ();
	Register_UnityEngine_Application_Quit ();
	void Register_UnityEngine_Application_CancelQuit ();
	Register_UnityEngine_Application_CancelQuit ();
	void Register_UnityEngine_Application_Unload ();
	Register_UnityEngine_Application_Unload ();
	void Register_UnityEngine_Application_get_isLoadingLevel ();
	Register_UnityEngine_Application_get_isLoadingLevel ();
	void Register_UnityEngine_Application_GetStreamProgressForLevelByName ();
	Register_UnityEngine_Application_GetStreamProgressForLevelByName ();
	void Register_UnityEngine_Application_GetStreamProgressForLevel ();
	Register_UnityEngine_Application_GetStreamProgressForLevel ();
	void Register_UnityEngine_Application_get_streamedBytes ();
	Register_UnityEngine_Application_get_streamedBytes ();
	void Register_UnityEngine_Application_CanStreamedLevelBeLoadedByName ();
	Register_UnityEngine_Application_CanStreamedLevelBeLoadedByName ();
	void Register_UnityEngine_Application_CanStreamedLevelBeLoaded ();
	Register_UnityEngine_Application_CanStreamedLevelBeLoaded ();
	void Register_UnityEngine_Application_get_isPlaying ();
	Register_UnityEngine_Application_get_isPlaying ();
	void Register_UnityEngine_Application_get_isEditor ();
	Register_UnityEngine_Application_get_isEditor ();
	void Register_UnityEngine_Application_get_isWebPlayer ();
	Register_UnityEngine_Application_get_isWebPlayer ();
	void Register_UnityEngine_Application_get_platform ();
	Register_UnityEngine_Application_get_platform ();
	void Register_UnityEngine_Application_CaptureScreenshot ();
	Register_UnityEngine_Application_CaptureScreenshot ();
	void Register_UnityEngine_Application_get_runInBackground ();
	Register_UnityEngine_Application_get_runInBackground ();
	void Register_UnityEngine_Application_set_runInBackground ();
	Register_UnityEngine_Application_set_runInBackground ();
	void Register_UnityEngine_Application_HasProLicense ();
	Register_UnityEngine_Application_HasProLicense ();
	void Register_UnityEngine_Application_HasAdvancedLicense ();
	Register_UnityEngine_Application_HasAdvancedLicense ();
	void Register_UnityEngine_Application_get_isBatchmode ();
	Register_UnityEngine_Application_get_isBatchmode ();
	void Register_UnityEngine_Application_get_isTestRun ();
	Register_UnityEngine_Application_get_isTestRun ();
	void Register_UnityEngine_Application_get_isHumanControllingUs ();
	Register_UnityEngine_Application_get_isHumanControllingUs ();
	void Register_UnityEngine_Application_HasARGV ();
	Register_UnityEngine_Application_HasARGV ();
	void Register_UnityEngine_Application_GetValueForARGV ();
	Register_UnityEngine_Application_GetValueForARGV ();
	void Register_UnityEngine_Application_DontDestroyOnLoad ();
	Register_UnityEngine_Application_DontDestroyOnLoad ();
	void Register_UnityEngine_Application_get_dataPath ();
	Register_UnityEngine_Application_get_dataPath ();
	void Register_UnityEngine_Application_get_streamingAssetsPath ();
	Register_UnityEngine_Application_get_streamingAssetsPath ();
	void Register_UnityEngine_Application_get_persistentDataPath ();
	Register_UnityEngine_Application_get_persistentDataPath ();
	void Register_UnityEngine_Application_get_temporaryCachePath ();
	Register_UnityEngine_Application_get_temporaryCachePath ();
	void Register_UnityEngine_Application_get_srcValue ();
	Register_UnityEngine_Application_get_srcValue ();
	void Register_UnityEngine_Application_get_absoluteURL ();
	Register_UnityEngine_Application_get_absoluteURL ();
	void Register_UnityEngine_Application_Internal_ExternalCall ();
	Register_UnityEngine_Application_Internal_ExternalCall ();
	void Register_UnityEngine_Application_get_unityVersion ();
	Register_UnityEngine_Application_get_unityVersion ();
	void Register_UnityEngine_Application_get_version ();
	Register_UnityEngine_Application_get_version ();
	void Register_UnityEngine_Application_get_installerName ();
	Register_UnityEngine_Application_get_installerName ();
	void Register_UnityEngine_Application_get_bundleIdentifier ();
	Register_UnityEngine_Application_get_bundleIdentifier ();
	void Register_UnityEngine_Application_get_installMode ();
	Register_UnityEngine_Application_get_installMode ();
	void Register_UnityEngine_Application_get_sandboxType ();
	Register_UnityEngine_Application_get_sandboxType ();
	void Register_UnityEngine_Application_get_productName ();
	Register_UnityEngine_Application_get_productName ();
	void Register_UnityEngine_Application_get_companyName ();
	Register_UnityEngine_Application_get_companyName ();
	void Register_UnityEngine_Application_get_cloudProjectId ();
	Register_UnityEngine_Application_get_cloudProjectId ();
	void Register_UnityEngine_Application_RequestAdvertisingIdentifierAsync ();
	Register_UnityEngine_Application_RequestAdvertisingIdentifierAsync ();
	void Register_UnityEngine_Application_get_webSecurityEnabled ();
	Register_UnityEngine_Application_get_webSecurityEnabled ();
	void Register_UnityEngine_Application_get_webSecurityHostUrl ();
	Register_UnityEngine_Application_get_webSecurityHostUrl ();
	void Register_UnityEngine_Application_OpenURL ();
	Register_UnityEngine_Application_OpenURL ();
	void Register_UnityEngine_Application_ForceCrash ();
	Register_UnityEngine_Application_ForceCrash ();
	void Register_UnityEngine_Application_get_targetFrameRate ();
	Register_UnityEngine_Application_get_targetFrameRate ();
	void Register_UnityEngine_Application_set_targetFrameRate ();
	Register_UnityEngine_Application_set_targetFrameRate ();
	void Register_UnityEngine_Application_get_systemLanguage ();
	Register_UnityEngine_Application_get_systemLanguage ();
	void Register_UnityEngine_Application_SetLogCallbackDefined ();
	Register_UnityEngine_Application_SetLogCallbackDefined ();
	void Register_UnityEngine_Application_get_stackTraceLogType ();
	Register_UnityEngine_Application_get_stackTraceLogType ();
	void Register_UnityEngine_Application_set_stackTraceLogType ();
	Register_UnityEngine_Application_set_stackTraceLogType ();
	void Register_UnityEngine_Application_GetStackTraceLogType ();
	Register_UnityEngine_Application_GetStackTraceLogType ();
	void Register_UnityEngine_Application_SetStackTraceLogType ();
	Register_UnityEngine_Application_SetStackTraceLogType ();
	void Register_UnityEngine_Application_get_backgroundLoadingPriority ();
	Register_UnityEngine_Application_get_backgroundLoadingPriority ();
	void Register_UnityEngine_Application_set_backgroundLoadingPriority ();
	Register_UnityEngine_Application_set_backgroundLoadingPriority ();
	void Register_UnityEngine_Application_get_internetReachability ();
	Register_UnityEngine_Application_get_internetReachability ();
	void Register_UnityEngine_Application_get_genuine ();
	Register_UnityEngine_Application_get_genuine ();
	void Register_UnityEngine_Application_get_genuineCheckAvailable ();
	Register_UnityEngine_Application_get_genuineCheckAvailable ();
	void Register_UnityEngine_Application_RequestUserAuthorization ();
	Register_UnityEngine_Application_RequestUserAuthorization ();
	void Register_UnityEngine_Application_HasUserAuthorization ();
	Register_UnityEngine_Application_HasUserAuthorization ();
	void Register_UnityEngine_Application_ReplyToUserAuthorizationRequest ();
	Register_UnityEngine_Application_ReplyToUserAuthorizationRequest ();
	void Register_UnityEngine_Application_GetUserAuthorizationRequestMode_Internal ();
	Register_UnityEngine_Application_GetUserAuthorizationRequestMode_Internal ();
	void Register_UnityEngine_Application_get_submitAnalytics ();
	Register_UnityEngine_Application_get_submitAnalytics ();
	void Register_UnityEngine_Application_get_isShowingSplashScreen ();
	Register_UnityEngine_Application_get_isShowingSplashScreen ();
	void Register_UnityEngine_AssetBundle_LoadFromFileAsync ();
	Register_UnityEngine_AssetBundle_LoadFromFileAsync ();
	void Register_UnityEngine_AssetBundle_LoadFromFile ();
	Register_UnityEngine_AssetBundle_LoadFromFile ();
	void Register_UnityEngine_AssetBundle_LoadFromMemoryAsync ();
	Register_UnityEngine_AssetBundle_LoadFromMemoryAsync ();
	void Register_UnityEngine_AssetBundle_LoadFromMemory ();
	Register_UnityEngine_AssetBundle_LoadFromMemory ();
	void Register_UnityEngine_AssetBundle_get_mainAsset ();
	Register_UnityEngine_AssetBundle_get_mainAsset ();
	void Register_UnityEngine_AssetBundle_get_isStreamedSceneAssetBundle ();
	Register_UnityEngine_AssetBundle_get_isStreamedSceneAssetBundle ();
	void Register_UnityEngine_AssetBundle_Contains ();
	Register_UnityEngine_AssetBundle_Contains ();
	void Register_UnityEngine_AssetBundle_Load ();
	Register_UnityEngine_AssetBundle_Load ();
	void Register_UnityEngine_AssetBundle_LoadAsync ();
	Register_UnityEngine_AssetBundle_LoadAsync ();
	void Register_UnityEngine_AssetBundle_LoadAll ();
	Register_UnityEngine_AssetBundle_LoadAll ();
	void Register_UnityEngine_AssetBundle_LoadAsset_Internal ();
	Register_UnityEngine_AssetBundle_LoadAsset_Internal ();
	void Register_UnityEngine_AssetBundle_LoadAssetAsync_Internal ();
	Register_UnityEngine_AssetBundle_LoadAssetAsync_Internal ();
	void Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal ();
	Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal ();
	void Register_UnityEngine_AssetBundle_LoadAssetWithSubAssetsAsync_Internal ();
	Register_UnityEngine_AssetBundle_LoadAssetWithSubAssetsAsync_Internal ();
	void Register_UnityEngine_AssetBundle_Unload ();
	Register_UnityEngine_AssetBundle_Unload ();
	void Register_UnityEngine_AssetBundle_GetAllAssetNames ();
	Register_UnityEngine_AssetBundle_GetAllAssetNames ();
	void Register_UnityEngine_AssetBundle_GetAllScenePaths ();
	Register_UnityEngine_AssetBundle_GetAllScenePaths ();
	void Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle ();
	Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle ();
	void Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks ();
	Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks ();
	void Register_UnityEngine_AssetBundleRequest_get_asset ();
	Register_UnityEngine_AssetBundleRequest_get_asset ();
	void Register_UnityEngine_AssetBundleRequest_get_allAssets ();
	Register_UnityEngine_AssetBundleRequest_get_allAssets ();
	void Register_UnityEngine_AsyncOperation_InternalDestroy ();
	Register_UnityEngine_AsyncOperation_InternalDestroy ();
	void Register_UnityEngine_AsyncOperation_get_isDone ();
	Register_UnityEngine_AsyncOperation_get_isDone ();
	void Register_UnityEngine_AsyncOperation_get_progress ();
	Register_UnityEngine_AsyncOperation_get_progress ();
	void Register_UnityEngine_AsyncOperation_get_priority ();
	Register_UnityEngine_AsyncOperation_get_priority ();
	void Register_UnityEngine_AsyncOperation_set_priority ();
	Register_UnityEngine_AsyncOperation_set_priority ();
	void Register_UnityEngine_AsyncOperation_get_allowSceneActivation ();
	Register_UnityEngine_AsyncOperation_get_allowSceneActivation ();
	void Register_UnityEngine_AsyncOperation_set_allowSceneActivation ();
	Register_UnityEngine_AsyncOperation_set_allowSceneActivation ();
	void Register_UnityEngine_AudioClip_get_length ();
	Register_UnityEngine_AudioClip_get_length ();
	void Register_UnityEngine_AudioClip_get_samples ();
	Register_UnityEngine_AudioClip_get_samples ();
	void Register_UnityEngine_AudioClip_get_channels ();
	Register_UnityEngine_AudioClip_get_channels ();
	void Register_UnityEngine_AudioClip_get_frequency ();
	Register_UnityEngine_AudioClip_get_frequency ();
	void Register_UnityEngine_AudioClip_get_isReadyToPlay ();
	Register_UnityEngine_AudioClip_get_isReadyToPlay ();
	void Register_UnityEngine_AudioClip_get_loadType ();
	Register_UnityEngine_AudioClip_get_loadType ();
	void Register_UnityEngine_AudioClip_LoadAudioData ();
	Register_UnityEngine_AudioClip_LoadAudioData ();
	void Register_UnityEngine_AudioClip_UnloadAudioData ();
	Register_UnityEngine_AudioClip_UnloadAudioData ();
	void Register_UnityEngine_AudioClip_get_preloadAudioData ();
	Register_UnityEngine_AudioClip_get_preloadAudioData ();
	void Register_UnityEngine_AudioClip_get_loadState ();
	Register_UnityEngine_AudioClip_get_loadState ();
	void Register_UnityEngine_AudioClip_get_loadInBackground ();
	Register_UnityEngine_AudioClip_get_loadInBackground ();
	void Register_UnityEngine_AudioClip_GetData ();
	Register_UnityEngine_AudioClip_GetData ();
	void Register_UnityEngine_AudioClip_SetData ();
	Register_UnityEngine_AudioClip_SetData ();
	void Register_UnityEngine_AudioClip_Construct_Internal ();
	Register_UnityEngine_AudioClip_Construct_Internal ();
	void Register_UnityEngine_AudioClip_Init_Internal ();
	Register_UnityEngine_AudioClip_Init_Internal ();
	void Register_UnityEngine_AudioListener_set_pause ();
	Register_UnityEngine_AudioListener_set_pause ();
	void Register_UnityEngine_AudioSettings_get_driverCapabilities ();
	Register_UnityEngine_AudioSettings_get_driverCapabilities ();
	void Register_UnityEngine_AudioSettings_get_speakerMode ();
	Register_UnityEngine_AudioSettings_get_speakerMode ();
	void Register_UnityEngine_AudioSettings_set_speakerMode ();
	Register_UnityEngine_AudioSettings_set_speakerMode ();
	void Register_UnityEngine_AudioSettings_get_profilerCaptureFlags ();
	Register_UnityEngine_AudioSettings_get_profilerCaptureFlags ();
	void Register_UnityEngine_AudioSettings_get_dspTime ();
	Register_UnityEngine_AudioSettings_get_dspTime ();
	void Register_UnityEngine_AudioSettings_get_outputSampleRate ();
	Register_UnityEngine_AudioSettings_get_outputSampleRate ();
	void Register_UnityEngine_AudioSettings_set_outputSampleRate ();
	Register_UnityEngine_AudioSettings_set_outputSampleRate ();
	void Register_UnityEngine_AudioSettings_GetDSPBufferSize ();
	Register_UnityEngine_AudioSettings_GetDSPBufferSize ();
	void Register_UnityEngine_AudioSettings_SetDSPBufferSize ();
	Register_UnityEngine_AudioSettings_SetDSPBufferSize ();
	void Register_UnityEngine_AudioSettings_INTERNAL_CALL_GetConfiguration ();
	Register_UnityEngine_AudioSettings_INTERNAL_CALL_GetConfiguration ();
	void Register_UnityEngine_AudioSettings_INTERNAL_CALL_Reset ();
	Register_UnityEngine_AudioSettings_INTERNAL_CALL_Reset ();
	void Register_UnityEngine_AudioSettings_get_unityAudioDisabled ();
	Register_UnityEngine_AudioSettings_get_unityAudioDisabled ();
	void Register_UnityEngine_AudioSource_PlayOneShot ();
	Register_UnityEngine_AudioSource_PlayOneShot ();
	void Register_UnityEngine_Behaviour_get_enabled ();
	Register_UnityEngine_Behaviour_get_enabled ();
	void Register_UnityEngine_Behaviour_set_enabled ();
	Register_UnityEngine_Behaviour_set_enabled ();
	void Register_UnityEngine_Behaviour_get_isActiveAndEnabled ();
	Register_UnityEngine_Behaviour_get_isActiveAndEnabled ();
	void Register_UnityEngine_BitStream_Serializeb ();
	Register_UnityEngine_BitStream_Serializeb ();
	void Register_UnityEngine_BitStream_Serializec ();
	Register_UnityEngine_BitStream_Serializec ();
	void Register_UnityEngine_BitStream_Serializes ();
	Register_UnityEngine_BitStream_Serializes ();
	void Register_UnityEngine_BitStream_Serializei ();
	Register_UnityEngine_BitStream_Serializei ();
	void Register_UnityEngine_BitStream_Serializef ();
	Register_UnityEngine_BitStream_Serializef ();
	void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializeq ();
	Register_UnityEngine_BitStream_INTERNAL_CALL_Serializeq ();
	void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializev ();
	Register_UnityEngine_BitStream_INTERNAL_CALL_Serializev ();
	void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializen ();
	Register_UnityEngine_BitStream_INTERNAL_CALL_Serializen ();
	void Register_UnityEngine_BitStream_get_isReading ();
	Register_UnityEngine_BitStream_get_isReading ();
	void Register_UnityEngine_BitStream_get_isWriting ();
	Register_UnityEngine_BitStream_get_isWriting ();
	void Register_UnityEngine_BitStream_Serialize ();
	Register_UnityEngine_BitStream_Serialize ();
	void Register_UnityEngine_Camera_get_fieldOfView ();
	Register_UnityEngine_Camera_get_fieldOfView ();
	void Register_UnityEngine_Camera_set_fieldOfView ();
	Register_UnityEngine_Camera_set_fieldOfView ();
	void Register_UnityEngine_Camera_get_nearClipPlane ();
	Register_UnityEngine_Camera_get_nearClipPlane ();
	void Register_UnityEngine_Camera_set_nearClipPlane ();
	Register_UnityEngine_Camera_set_nearClipPlane ();
	void Register_UnityEngine_Camera_get_farClipPlane ();
	Register_UnityEngine_Camera_get_farClipPlane ();
	void Register_UnityEngine_Camera_set_farClipPlane ();
	Register_UnityEngine_Camera_set_farClipPlane ();
	void Register_UnityEngine_Camera_get_renderingPath ();
	Register_UnityEngine_Camera_get_renderingPath ();
	void Register_UnityEngine_Camera_set_renderingPath ();
	Register_UnityEngine_Camera_set_renderingPath ();
	void Register_UnityEngine_Camera_get_actualRenderingPath ();
	Register_UnityEngine_Camera_get_actualRenderingPath ();
	void Register_UnityEngine_Camera_get_hdr ();
	Register_UnityEngine_Camera_get_hdr ();
	void Register_UnityEngine_Camera_set_hdr ();
	Register_UnityEngine_Camera_set_hdr ();
	void Register_UnityEngine_Camera_GetHDRWarnings ();
	Register_UnityEngine_Camera_GetHDRWarnings ();
	void Register_UnityEngine_Camera_get_orthographicSize ();
	Register_UnityEngine_Camera_get_orthographicSize ();
	void Register_UnityEngine_Camera_set_orthographicSize ();
	Register_UnityEngine_Camera_set_orthographicSize ();
	void Register_UnityEngine_Camera_get_orthographic ();
	Register_UnityEngine_Camera_get_orthographic ();
	void Register_UnityEngine_Camera_set_orthographic ();
	Register_UnityEngine_Camera_set_orthographic ();
	void Register_UnityEngine_Camera_get_opaqueSortMode ();
	Register_UnityEngine_Camera_get_opaqueSortMode ();
	void Register_UnityEngine_Camera_set_opaqueSortMode ();
	Register_UnityEngine_Camera_set_opaqueSortMode ();
	void Register_UnityEngine_Camera_get_transparencySortMode ();
	Register_UnityEngine_Camera_get_transparencySortMode ();
	void Register_UnityEngine_Camera_set_transparencySortMode ();
	Register_UnityEngine_Camera_set_transparencySortMode ();
	void Register_UnityEngine_Camera_get_depth ();
	Register_UnityEngine_Camera_get_depth ();
	void Register_UnityEngine_Camera_set_depth ();
	Register_UnityEngine_Camera_set_depth ();
	void Register_UnityEngine_Camera_get_aspect ();
	Register_UnityEngine_Camera_get_aspect ();
	void Register_UnityEngine_Camera_set_aspect ();
	Register_UnityEngine_Camera_set_aspect ();
	void Register_UnityEngine_Camera_get_cullingMask ();
	Register_UnityEngine_Camera_get_cullingMask ();
	void Register_UnityEngine_Camera_set_cullingMask ();
	Register_UnityEngine_Camera_set_cullingMask ();
	void Register_UnityEngine_Camera_get_PreviewCullingLayer ();
	Register_UnityEngine_Camera_get_PreviewCullingLayer ();
	void Register_UnityEngine_Camera_get_eventMask ();
	Register_UnityEngine_Camera_get_eventMask ();
	void Register_UnityEngine_Camera_set_eventMask ();
	Register_UnityEngine_Camera_set_eventMask ();
	void Register_UnityEngine_Camera_INTERNAL_get_backgroundColor ();
	Register_UnityEngine_Camera_INTERNAL_get_backgroundColor ();
	void Register_UnityEngine_Camera_INTERNAL_set_backgroundColor ();
	Register_UnityEngine_Camera_INTERNAL_set_backgroundColor ();
	void Register_UnityEngine_Camera_INTERNAL_get_rect ();
	Register_UnityEngine_Camera_INTERNAL_get_rect ();
	void Register_UnityEngine_Camera_INTERNAL_set_rect ();
	Register_UnityEngine_Camera_INTERNAL_set_rect ();
	void Register_UnityEngine_Camera_INTERNAL_get_pixelRect ();
	Register_UnityEngine_Camera_INTERNAL_get_pixelRect ();
	void Register_UnityEngine_Camera_INTERNAL_set_pixelRect ();
	Register_UnityEngine_Camera_INTERNAL_set_pixelRect ();
	void Register_UnityEngine_Camera_get_targetTexture ();
	Register_UnityEngine_Camera_get_targetTexture ();
	void Register_UnityEngine_Camera_set_targetTexture ();
	Register_UnityEngine_Camera_set_targetTexture ();
	void Register_UnityEngine_Camera_SetTargetBuffersImpl ();
	Register_UnityEngine_Camera_SetTargetBuffersImpl ();
	void Register_UnityEngine_Camera_SetTargetBuffersMRTImpl ();
	Register_UnityEngine_Camera_SetTargetBuffersMRTImpl ();
	void Register_UnityEngine_Camera_get_pixelWidth ();
	Register_UnityEngine_Camera_get_pixelWidth ();
	void Register_UnityEngine_Camera_get_pixelHeight ();
	Register_UnityEngine_Camera_get_pixelHeight ();
	void Register_UnityEngine_Camera_INTERNAL_get_cameraToWorldMatrix ();
	Register_UnityEngine_Camera_INTERNAL_get_cameraToWorldMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_get_worldToCameraMatrix ();
	Register_UnityEngine_Camera_INTERNAL_get_worldToCameraMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_set_worldToCameraMatrix ();
	Register_UnityEngine_Camera_INTERNAL_set_worldToCameraMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ResetWorldToCameraMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ResetWorldToCameraMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_get_projectionMatrix ();
	Register_UnityEngine_Camera_INTERNAL_get_projectionMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_set_projectionMatrix ();
	Register_UnityEngine_Camera_INTERNAL_set_projectionMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_get_nonJitteredProjectionMatrix ();
	Register_UnityEngine_Camera_INTERNAL_get_nonJitteredProjectionMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_set_nonJitteredProjectionMatrix ();
	Register_UnityEngine_Camera_INTERNAL_set_nonJitteredProjectionMatrix ();
	void Register_UnityEngine_Camera_get_useJitteredProjectionMatrixForTransparentRendering ();
	Register_UnityEngine_Camera_get_useJitteredProjectionMatrixForTransparentRendering ();
	void Register_UnityEngine_Camera_set_useJitteredProjectionMatrixForTransparentRendering ();
	Register_UnityEngine_Camera_set_useJitteredProjectionMatrixForTransparentRendering ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ResetProjectionMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ResetProjectionMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ResetAspect ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ResetAspect ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ResetFieldOfView ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ResetFieldOfView ();
	void Register_UnityEngine_Camera_INTERNAL_get_velocity ();
	Register_UnityEngine_Camera_INTERNAL_get_velocity ();
	void Register_UnityEngine_Camera_get_clearFlags ();
	Register_UnityEngine_Camera_get_clearFlags ();
	void Register_UnityEngine_Camera_set_clearFlags ();
	Register_UnityEngine_Camera_set_clearFlags ();
	void Register_UnityEngine_Camera_get_stereoEnabled ();
	Register_UnityEngine_Camera_get_stereoEnabled ();
	void Register_UnityEngine_Camera_get_stereoSeparation ();
	Register_UnityEngine_Camera_get_stereoSeparation ();
	void Register_UnityEngine_Camera_set_stereoSeparation ();
	Register_UnityEngine_Camera_set_stereoSeparation ();
	void Register_UnityEngine_Camera_get_stereoConvergence ();
	Register_UnityEngine_Camera_get_stereoConvergence ();
	void Register_UnityEngine_Camera_set_stereoConvergence ();
	Register_UnityEngine_Camera_set_stereoConvergence ();
	void Register_UnityEngine_Camera_get_cameraType ();
	Register_UnityEngine_Camera_get_cameraType ();
	void Register_UnityEngine_Camera_set_cameraType ();
	Register_UnityEngine_Camera_set_cameraType ();
	void Register_UnityEngine_Camera_get_stereoMirrorMode ();
	Register_UnityEngine_Camera_get_stereoMirrorMode ();
	void Register_UnityEngine_Camera_set_stereoMirrorMode ();
	Register_UnityEngine_Camera_set_stereoMirrorMode ();
	void Register_UnityEngine_Camera_GetStereoViewMatrices ();
	Register_UnityEngine_Camera_GetStereoViewMatrices ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_GetStereoViewMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_GetStereoViewMatrix ();
	void Register_UnityEngine_Camera_get_stereoTargetEye ();
	Register_UnityEngine_Camera_get_stereoTargetEye ();
	void Register_UnityEngine_Camera_set_stereoTargetEye ();
	Register_UnityEngine_Camera_set_stereoTargetEye ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoViewMatrices ();
	Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoViewMatrices ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoViewMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoViewMatrix ();
	void Register_UnityEngine_Camera_ResetStereoViewMatrices ();
	Register_UnityEngine_Camera_ResetStereoViewMatrices ();
	void Register_UnityEngine_Camera_GetStereoProjectionMatrices ();
	Register_UnityEngine_Camera_GetStereoProjectionMatrices ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_GetStereoProjectionMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_GetStereoProjectionMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoProjectionMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoProjectionMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoProjectionMatrices ();
	Register_UnityEngine_Camera_INTERNAL_CALL_SetStereoProjectionMatrices ();
	void Register_UnityEngine_Camera_get_stereoActiveEye ();
	Register_UnityEngine_Camera_get_stereoActiveEye ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_CalculateFrustumCornersInternal ();
	Register_UnityEngine_Camera_INTERNAL_CALL_CalculateFrustumCornersInternal ();
	void Register_UnityEngine_Camera_ResetStereoProjectionMatrices ();
	Register_UnityEngine_Camera_ResetStereoProjectionMatrices ();
	void Register_UnityEngine_Camera_get_targetDisplay ();
	Register_UnityEngine_Camera_get_targetDisplay ();
	void Register_UnityEngine_Camera_set_targetDisplay ();
	Register_UnityEngine_Camera_set_targetDisplay ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint ();
	Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint ();
	Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToWorldPoint ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToWorldPoint ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToScreenPoint ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ViewportToScreenPoint ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ViewportPointToRay ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ViewportPointToRay ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay ();
	void Register_UnityEngine_Camera_get_main ();
	Register_UnityEngine_Camera_get_main ();
	void Register_UnityEngine_Camera_get_current ();
	Register_UnityEngine_Camera_get_current ();
	void Register_UnityEngine_Camera_get_allCameras ();
	Register_UnityEngine_Camera_get_allCameras ();
	void Register_UnityEngine_Camera_get_allCamerasCount ();
	Register_UnityEngine_Camera_get_allCamerasCount ();
	void Register_UnityEngine_Camera_GetAllCameras ();
	Register_UnityEngine_Camera_GetAllCameras ();
	void Register_UnityEngine_Camera_Render ();
	Register_UnityEngine_Camera_Render ();
	void Register_UnityEngine_Camera_RenderWithShader ();
	Register_UnityEngine_Camera_RenderWithShader ();
	void Register_UnityEngine_Camera_SetReplacementShader ();
	Register_UnityEngine_Camera_SetReplacementShader ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ResetReplacementShader ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ResetReplacementShader ();
	void Register_UnityEngine_Camera_get_useOcclusionCulling ();
	Register_UnityEngine_Camera_get_useOcclusionCulling ();
	void Register_UnityEngine_Camera_set_useOcclusionCulling ();
	Register_UnityEngine_Camera_set_useOcclusionCulling ();
	void Register_UnityEngine_Camera_INTERNAL_get_cullingMatrix ();
	Register_UnityEngine_Camera_INTERNAL_get_cullingMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_set_cullingMatrix ();
	Register_UnityEngine_Camera_INTERNAL_set_cullingMatrix ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_ResetCullingMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_ResetCullingMatrix ();
	void Register_UnityEngine_Camera_RenderDontRestore ();
	Register_UnityEngine_Camera_RenderDontRestore ();
	void Register_UnityEngine_Camera_SetupCurrent ();
	Register_UnityEngine_Camera_SetupCurrent ();
	void Register_UnityEngine_Camera_Internal_RenderToCubemapRT ();
	Register_UnityEngine_Camera_Internal_RenderToCubemapRT ();
	void Register_UnityEngine_Camera_Internal_RenderToCubemapTexture ();
	Register_UnityEngine_Camera_Internal_RenderToCubemapTexture ();
	void Register_UnityEngine_Camera_get_layerCullDistances ();
	Register_UnityEngine_Camera_get_layerCullDistances ();
	void Register_UnityEngine_Camera_set_layerCullDistances ();
	Register_UnityEngine_Camera_set_layerCullDistances ();
	void Register_UnityEngine_Camera_get_layerCullSpherical ();
	Register_UnityEngine_Camera_get_layerCullSpherical ();
	void Register_UnityEngine_Camera_set_layerCullSpherical ();
	Register_UnityEngine_Camera_set_layerCullSpherical ();
	void Register_UnityEngine_Camera_CopyFrom ();
	Register_UnityEngine_Camera_CopyFrom ();
	void Register_UnityEngine_Camera_get_depthTextureMode ();
	Register_UnityEngine_Camera_get_depthTextureMode ();
	void Register_UnityEngine_Camera_set_depthTextureMode ();
	Register_UnityEngine_Camera_set_depthTextureMode ();
	void Register_UnityEngine_Camera_get_clearStencilAfterLightingPass ();
	Register_UnityEngine_Camera_get_clearStencilAfterLightingPass ();
	void Register_UnityEngine_Camera_set_clearStencilAfterLightingPass ();
	Register_UnityEngine_Camera_set_clearStencilAfterLightingPass ();
	void Register_UnityEngine_Camera_IsFiltered ();
	Register_UnityEngine_Camera_IsFiltered ();
	void Register_UnityEngine_Camera_AddCommandBuffer ();
	Register_UnityEngine_Camera_AddCommandBuffer ();
	void Register_UnityEngine_Camera_RemoveCommandBuffer ();
	Register_UnityEngine_Camera_RemoveCommandBuffer ();
	void Register_UnityEngine_Camera_RemoveCommandBuffers ();
	Register_UnityEngine_Camera_RemoveCommandBuffers ();
	void Register_UnityEngine_Camera_RemoveAllCommandBuffers ();
	Register_UnityEngine_Camera_RemoveAllCommandBuffers ();
	void Register_UnityEngine_Camera_GetCommandBuffers ();
	Register_UnityEngine_Camera_GetCommandBuffers ();
	void Register_UnityEngine_Camera_get_commandBufferCount ();
	Register_UnityEngine_Camera_get_commandBufferCount ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry ();
	Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D ();
	Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D ();
	void Register_UnityEngine_Camera_INTERNAL_CALL_CalculateObliqueMatrix ();
	Register_UnityEngine_Camera_INTERNAL_CALL_CalculateObliqueMatrix ();
	void Register_UnityEngine_Canvas_get_renderMode ();
	Register_UnityEngine_Canvas_get_renderMode ();
	void Register_UnityEngine_Canvas_set_renderMode ();
	Register_UnityEngine_Canvas_set_renderMode ();
	void Register_UnityEngine_Canvas_get_isRootCanvas ();
	Register_UnityEngine_Canvas_get_isRootCanvas ();
	void Register_UnityEngine_Canvas_get_worldCamera ();
	Register_UnityEngine_Canvas_get_worldCamera ();
	void Register_UnityEngine_Canvas_set_worldCamera ();
	Register_UnityEngine_Canvas_set_worldCamera ();
	void Register_UnityEngine_Canvas_INTERNAL_get_pixelRect ();
	Register_UnityEngine_Canvas_INTERNAL_get_pixelRect ();
	void Register_UnityEngine_Canvas_get_scaleFactor ();
	Register_UnityEngine_Canvas_get_scaleFactor ();
	void Register_UnityEngine_Canvas_set_scaleFactor ();
	Register_UnityEngine_Canvas_set_scaleFactor ();
	void Register_UnityEngine_Canvas_get_referencePixelsPerUnit ();
	Register_UnityEngine_Canvas_get_referencePixelsPerUnit ();
	void Register_UnityEngine_Canvas_set_referencePixelsPerUnit ();
	Register_UnityEngine_Canvas_set_referencePixelsPerUnit ();
	void Register_UnityEngine_Canvas_get_overridePixelPerfect ();
	Register_UnityEngine_Canvas_get_overridePixelPerfect ();
	void Register_UnityEngine_Canvas_set_overridePixelPerfect ();
	Register_UnityEngine_Canvas_set_overridePixelPerfect ();
	void Register_UnityEngine_Canvas_get_pixelPerfect ();
	Register_UnityEngine_Canvas_get_pixelPerfect ();
	void Register_UnityEngine_Canvas_set_pixelPerfect ();
	Register_UnityEngine_Canvas_set_pixelPerfect ();
	void Register_UnityEngine_Canvas_get_planeDistance ();
	Register_UnityEngine_Canvas_get_planeDistance ();
	void Register_UnityEngine_Canvas_set_planeDistance ();
	Register_UnityEngine_Canvas_set_planeDistance ();
	void Register_UnityEngine_Canvas_get_renderOrder ();
	Register_UnityEngine_Canvas_get_renderOrder ();
	void Register_UnityEngine_Canvas_get_overrideSorting ();
	Register_UnityEngine_Canvas_get_overrideSorting ();
	void Register_UnityEngine_Canvas_set_overrideSorting ();
	Register_UnityEngine_Canvas_set_overrideSorting ();
	void Register_UnityEngine_Canvas_get_sortingOrder ();
	Register_UnityEngine_Canvas_get_sortingOrder ();
	void Register_UnityEngine_Canvas_set_sortingOrder ();
	Register_UnityEngine_Canvas_set_sortingOrder ();
	void Register_UnityEngine_Canvas_get_targetDisplay ();
	Register_UnityEngine_Canvas_get_targetDisplay ();
	void Register_UnityEngine_Canvas_set_targetDisplay ();
	Register_UnityEngine_Canvas_set_targetDisplay ();
	void Register_UnityEngine_Canvas_get_sortingGridNormalizedSize ();
	Register_UnityEngine_Canvas_get_sortingGridNormalizedSize ();
	void Register_UnityEngine_Canvas_set_sortingGridNormalizedSize ();
	Register_UnityEngine_Canvas_set_sortingGridNormalizedSize ();
	void Register_UnityEngine_Canvas_get_normalizedSortingGridSize ();
	Register_UnityEngine_Canvas_get_normalizedSortingGridSize ();
	void Register_UnityEngine_Canvas_set_normalizedSortingGridSize ();
	Register_UnityEngine_Canvas_set_normalizedSortingGridSize ();
	void Register_UnityEngine_Canvas_get_sortingLayerID ();
	Register_UnityEngine_Canvas_get_sortingLayerID ();
	void Register_UnityEngine_Canvas_set_sortingLayerID ();
	Register_UnityEngine_Canvas_set_sortingLayerID ();
	void Register_UnityEngine_Canvas_get_cachedSortingLayerValue ();
	Register_UnityEngine_Canvas_get_cachedSortingLayerValue ();
	void Register_UnityEngine_Canvas_get_sortingLayerName ();
	Register_UnityEngine_Canvas_get_sortingLayerName ();
	void Register_UnityEngine_Canvas_set_sortingLayerName ();
	Register_UnityEngine_Canvas_set_sortingLayerName ();
	void Register_UnityEngine_Canvas_get_rootCanvas ();
	Register_UnityEngine_Canvas_get_rootCanvas ();
	void Register_UnityEngine_Canvas_GetDefaultCanvasMaterial ();
	Register_UnityEngine_Canvas_GetDefaultCanvasMaterial ();
	void Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial ();
	Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial ();
	void Register_UnityEngine_Canvas_GetDefaultCanvasTextMaterial ();
	Register_UnityEngine_Canvas_GetDefaultCanvasTextMaterial ();
	void Register_UnityEngine_CanvasGroup_get_alpha ();
	Register_UnityEngine_CanvasGroup_get_alpha ();
	void Register_UnityEngine_CanvasGroup_set_alpha ();
	Register_UnityEngine_CanvasGroup_set_alpha ();
	void Register_UnityEngine_CanvasGroup_get_interactable ();
	Register_UnityEngine_CanvasGroup_get_interactable ();
	void Register_UnityEngine_CanvasGroup_get_blocksRaycasts ();
	Register_UnityEngine_CanvasGroup_get_blocksRaycasts ();
	void Register_UnityEngine_CanvasGroup_get_ignoreParentGroups ();
	Register_UnityEngine_CanvasGroup_get_ignoreParentGroups ();
	void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor ();
	Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor ();
	void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor ();
	Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor ();
	void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping ();
	Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping ();
	void Register_UnityEngine_CanvasRenderer_DisableRectClipping ();
	Register_UnityEngine_CanvasRenderer_DisableRectClipping ();
	void Register_UnityEngine_CanvasRenderer_set_hasPopInstruction ();
	Register_UnityEngine_CanvasRenderer_set_hasPopInstruction ();
	void Register_UnityEngine_CanvasRenderer_get_materialCount ();
	Register_UnityEngine_CanvasRenderer_get_materialCount ();
	void Register_UnityEngine_CanvasRenderer_set_materialCount ();
	Register_UnityEngine_CanvasRenderer_set_materialCount ();
	void Register_UnityEngine_CanvasRenderer_SetMaterial ();
	Register_UnityEngine_CanvasRenderer_SetMaterial ();
	void Register_UnityEngine_CanvasRenderer_set_popMaterialCount ();
	Register_UnityEngine_CanvasRenderer_set_popMaterialCount ();
	void Register_UnityEngine_CanvasRenderer_SetPopMaterial ();
	Register_UnityEngine_CanvasRenderer_SetPopMaterial ();
	void Register_UnityEngine_CanvasRenderer_SetTexture ();
	Register_UnityEngine_CanvasRenderer_SetTexture ();
	void Register_UnityEngine_CanvasRenderer_SetAlphaTexture ();
	Register_UnityEngine_CanvasRenderer_SetAlphaTexture ();
	void Register_UnityEngine_CanvasRenderer_SetMesh ();
	Register_UnityEngine_CanvasRenderer_SetMesh ();
	void Register_UnityEngine_CanvasRenderer_Clear ();
	Register_UnityEngine_CanvasRenderer_Clear ();
	void Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal ();
	Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal ();
	void Register_UnityEngine_CanvasRenderer_SplitIndiciesStreamsInternal ();
	Register_UnityEngine_CanvasRenderer_SplitIndiciesStreamsInternal ();
	void Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal ();
	Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal ();
	void Register_UnityEngine_CanvasRenderer_get_cull ();
	Register_UnityEngine_CanvasRenderer_get_cull ();
	void Register_UnityEngine_CanvasRenderer_set_cull ();
	Register_UnityEngine_CanvasRenderer_set_cull ();
	void Register_UnityEngine_CanvasRenderer_get_absoluteDepth ();
	Register_UnityEngine_CanvasRenderer_get_absoluteDepth ();
	void Register_UnityEngine_CanvasRenderer_get_hasMoved ();
	Register_UnityEngine_CanvasRenderer_get_hasMoved ();
	void Register_UnityEngine_CharacterController_INTERNAL_CALL_Move ();
	Register_UnityEngine_CharacterController_INTERNAL_CALL_Move ();
	void Register_UnityEngine_CharacterController_INTERNAL_get_velocity ();
	Register_UnityEngine_CharacterController_INTERNAL_get_velocity ();
	void Register_UnityEngine_Collider_set_enabled ();
	Register_UnityEngine_Collider_set_enabled ();
	void Register_UnityEngine_Collider_get_attachedRigidbody ();
	Register_UnityEngine_Collider_get_attachedRigidbody ();
	void Register_UnityEngine_Collider2D_get_attachedRigidbody ();
	Register_UnityEngine_Collider2D_get_attachedRigidbody ();
	void Register_UnityEngine_Collider2D_INTERNAL_get_bounds ();
	Register_UnityEngine_Collider2D_INTERNAL_get_bounds ();
	void Register_UnityEngine_Component_get_transform ();
	Register_UnityEngine_Component_get_transform ();
	void Register_UnityEngine_Component_get_gameObject ();
	Register_UnityEngine_Component_get_gameObject ();
	void Register_UnityEngine_Component_GetComponentFastPath ();
	Register_UnityEngine_Component_GetComponentFastPath ();
	void Register_UnityEngine_Component_GetComponent ();
	Register_UnityEngine_Component_GetComponent ();
	void Register_UnityEngine_Component_GetComponentsForListInternal ();
	Register_UnityEngine_Component_GetComponentsForListInternal ();
	void Register_UnityEngine_Component_CompareTag ();
	Register_UnityEngine_Component_CompareTag ();
	void Register_UnityEngine_Component_SendMessageUpwards ();
	Register_UnityEngine_Component_SendMessageUpwards ();
	void Register_UnityEngine_Component_SendMessage ();
	Register_UnityEngine_Component_SendMessage ();
	void Register_UnityEngine_Component_BroadcastMessage ();
	Register_UnityEngine_Component_BroadcastMessage ();
	void Register_UnityEngine_Coroutine_ReleaseCoroutine ();
	Register_UnityEngine_Coroutine_ReleaseCoroutine ();
	void Register_UnityEngine_CullingGroup_Dispose ();
	Register_UnityEngine_CullingGroup_Dispose ();
	void Register_UnityEngine_CullingGroup_get_enabled ();
	Register_UnityEngine_CullingGroup_get_enabled ();
	void Register_UnityEngine_CullingGroup_set_enabled ();
	Register_UnityEngine_CullingGroup_set_enabled ();
	void Register_UnityEngine_CullingGroup_get_targetCamera ();
	Register_UnityEngine_CullingGroup_get_targetCamera ();
	void Register_UnityEngine_CullingGroup_set_targetCamera ();
	Register_UnityEngine_CullingGroup_set_targetCamera ();
	void Register_UnityEngine_CullingGroup_SetBoundingSpheres ();
	Register_UnityEngine_CullingGroup_SetBoundingSpheres ();
	void Register_UnityEngine_CullingGroup_SetBoundingSphereCount ();
	Register_UnityEngine_CullingGroup_SetBoundingSphereCount ();
	void Register_UnityEngine_CullingGroup_EraseSwapBack ();
	Register_UnityEngine_CullingGroup_EraseSwapBack ();
	void Register_UnityEngine_CullingGroup_QueryIndices ();
	Register_UnityEngine_CullingGroup_QueryIndices ();
	void Register_UnityEngine_CullingGroup_IsVisible ();
	Register_UnityEngine_CullingGroup_IsVisible ();
	void Register_UnityEngine_CullingGroup_GetDistance ();
	Register_UnityEngine_CullingGroup_GetDistance ();
	void Register_UnityEngine_CullingGroup_SetBoundingDistances ();
	Register_UnityEngine_CullingGroup_SetBoundingDistances ();
	void Register_UnityEngine_CullingGroup_INTERNAL_CALL_SetDistanceReferencePoint ();
	Register_UnityEngine_CullingGroup_INTERNAL_CALL_SetDistanceReferencePoint ();
	void Register_UnityEngine_CullingGroup_SetDistanceReferencePoint ();
	Register_UnityEngine_CullingGroup_SetDistanceReferencePoint ();
	void Register_UnityEngine_CullingGroup_Init ();
	Register_UnityEngine_CullingGroup_Init ();
	void Register_UnityEngine_CullingGroup_FinalizerFailure ();
	Register_UnityEngine_CullingGroup_FinalizerFailure ();
	void Register_UnityEngine_Cursor_get_lockState ();
	Register_UnityEngine_Cursor_get_lockState ();
	void Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine ();
	Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine ();
	void Register_UnityEngine_DebugLogHandler_Internal_Log ();
	Register_UnityEngine_DebugLogHandler_Internal_Log ();
	void Register_UnityEngine_DebugLogHandler_Internal_LogException ();
	Register_UnityEngine_DebugLogHandler_Internal_LogException ();
	void Register_UnityEngine_Display_GetSystemExtImpl ();
	Register_UnityEngine_Display_GetSystemExtImpl ();
	void Register_UnityEngine_Display_GetRenderingExtImpl ();
	Register_UnityEngine_Display_GetRenderingExtImpl ();
	void Register_UnityEngine_Display_GetRenderingBuffersImpl ();
	Register_UnityEngine_Display_GetRenderingBuffersImpl ();
	void Register_UnityEngine_Display_SetRenderingResolutionImpl ();
	Register_UnityEngine_Display_SetRenderingResolutionImpl ();
	void Register_UnityEngine_Display_ActivateDisplayImpl ();
	Register_UnityEngine_Display_ActivateDisplayImpl ();
	void Register_UnityEngine_Display_SetParamsImpl ();
	Register_UnityEngine_Display_SetParamsImpl ();
	void Register_UnityEngine_Display_RelativeMouseAtImpl ();
	Register_UnityEngine_Display_RelativeMouseAtImpl ();
	void Register_UnityEngine_Event_Init ();
	Register_UnityEngine_Event_Init ();
	void Register_UnityEngine_Event_Cleanup ();
	Register_UnityEngine_Event_Cleanup ();
	void Register_UnityEngine_Event_InitCopy ();
	Register_UnityEngine_Event_InitCopy ();
	void Register_UnityEngine_Event_InitPtr ();
	Register_UnityEngine_Event_InitPtr ();
	void Register_UnityEngine_Event_get_rawType ();
	Register_UnityEngine_Event_get_rawType ();
	void Register_UnityEngine_Event_get_type ();
	Register_UnityEngine_Event_get_type ();
	void Register_UnityEngine_Event_set_type ();
	Register_UnityEngine_Event_set_type ();
	void Register_UnityEngine_Event_GetTypeForControl ();
	Register_UnityEngine_Event_GetTypeForControl ();
	void Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMousePosition ();
	Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMousePosition ();
	void Register_UnityEngine_Event_Internal_GetMousePosition ();
	Register_UnityEngine_Event_Internal_GetMousePosition ();
	void Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMouseDelta ();
	Register_UnityEngine_Event_INTERNAL_CALL_Internal_SetMouseDelta ();
	void Register_UnityEngine_Event_Internal_GetMouseDelta ();
	Register_UnityEngine_Event_Internal_GetMouseDelta ();
	void Register_UnityEngine_Event_get_button ();
	Register_UnityEngine_Event_get_button ();
	void Register_UnityEngine_Event_set_button ();
	Register_UnityEngine_Event_set_button ();
	void Register_UnityEngine_Event_get_modifiers ();
	Register_UnityEngine_Event_get_modifiers ();
	void Register_UnityEngine_Event_set_modifiers ();
	Register_UnityEngine_Event_set_modifiers ();
	void Register_UnityEngine_Event_get_pressure ();
	Register_UnityEngine_Event_get_pressure ();
	void Register_UnityEngine_Event_set_pressure ();
	Register_UnityEngine_Event_set_pressure ();
	void Register_UnityEngine_Event_get_clickCount ();
	Register_UnityEngine_Event_get_clickCount ();
	void Register_UnityEngine_Event_set_clickCount ();
	Register_UnityEngine_Event_set_clickCount ();
	void Register_UnityEngine_Event_get_character ();
	Register_UnityEngine_Event_get_character ();
	void Register_UnityEngine_Event_set_character ();
	Register_UnityEngine_Event_set_character ();
	void Register_UnityEngine_Event_get_commandName ();
	Register_UnityEngine_Event_get_commandName ();
	void Register_UnityEngine_Event_set_commandName ();
	Register_UnityEngine_Event_set_commandName ();
	void Register_UnityEngine_Event_get_keyCode ();
	Register_UnityEngine_Event_get_keyCode ();
	void Register_UnityEngine_Event_set_keyCode ();
	Register_UnityEngine_Event_set_keyCode ();
	void Register_UnityEngine_Event_Internal_SetNativeEvent ();
	Register_UnityEngine_Event_Internal_SetNativeEvent ();
	void Register_UnityEngine_Event_get_displayIndex ();
	Register_UnityEngine_Event_get_displayIndex ();
	void Register_UnityEngine_Event_set_displayIndex ();
	Register_UnityEngine_Event_set_displayIndex ();
	void Register_UnityEngine_Event_Internal_Use ();
	Register_UnityEngine_Event_Internal_Use ();
	void Register_UnityEngine_Event_PopEvent ();
	Register_UnityEngine_Event_PopEvent ();
	void Register_UnityEngine_Event_GetEventCount ();
	Register_UnityEngine_Event_GetEventCount ();
	void Register_UnityEngine_Experimental_Director_GenericMixerPlayable_InternalCreate ();
	Register_UnityEngine_Experimental_Director_GenericMixerPlayable_InternalCreate ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_IsValidInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_IsValidInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_InternalCreate ();
	Register_UnityEngine_Experimental_Director_Playable_InternalCreate ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_CanChangeInputsInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_CanChangeInputsInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_CanSetWeightsInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_CanSetWeightsInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_CanDestroyInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_CanDestroyInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetPlayStateInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetPlayStateInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetPlayStateInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetPlayStateInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetTimeInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetTimeInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetTimeInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetTimeInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetDurationInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetDurationInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetDurationInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetDurationInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetInputCountInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetInputCountInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetOutputCountInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetOutputCountInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetInputInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetInputInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetOutputInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetOutputInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetInputWeightFromIndexInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetInputWeightFromIndexInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetInputWeightInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_SetInputWeightInternal ();
	void Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetInputWeightInternal ();
	Register_UnityEngine_Experimental_Director_Playable_INTERNAL_CALL_GetInputWeightInternal ();
	void Register_UnityEngine_Experimental_Director_Playables_CastToInternal ();
	Register_UnityEngine_Experimental_Director_Playables_CastToInternal ();
	void Register_UnityEngine_Experimental_Director_Playables_GetTypeOfInternal ();
	Register_UnityEngine_Experimental_Director_Playables_GetTypeOfInternal ();
	void Register_UnityEngine_Experimental_Director_Playables_INTERNAL_CALL_InternalDestroy ();
	Register_UnityEngine_Experimental_Director_Playables_INTERNAL_CALL_InternalDestroy ();
	void Register_UnityEngine_Experimental_Director_Playables_INTERNAL_CALL_ConnectInternal ();
	Register_UnityEngine_Experimental_Director_Playables_INTERNAL_CALL_ConnectInternal ();
	void Register_UnityEngine_Experimental_Director_Playables_INTERNAL_CALL_DisconnectInternal ();
	Register_UnityEngine_Experimental_Director_Playables_INTERNAL_CALL_DisconnectInternal ();
	void Register_UnityEngine_Font_GetOSInstalledFontNames ();
	Register_UnityEngine_Font_GetOSInstalledFontNames ();
	void Register_UnityEngine_Font_Internal_CreateFont ();
	Register_UnityEngine_Font_Internal_CreateFont ();
	void Register_UnityEngine_Font_Internal_CreateDynamicFont ();
	Register_UnityEngine_Font_Internal_CreateDynamicFont ();
	void Register_UnityEngine_Font_get_material ();
	Register_UnityEngine_Font_get_material ();
	void Register_UnityEngine_Font_set_material ();
	Register_UnityEngine_Font_set_material ();
	void Register_UnityEngine_Font_HasCharacter ();
	Register_UnityEngine_Font_HasCharacter ();
	void Register_UnityEngine_Font_get_fontNames ();
	Register_UnityEngine_Font_get_fontNames ();
	void Register_UnityEngine_Font_set_fontNames ();
	Register_UnityEngine_Font_set_fontNames ();
	void Register_UnityEngine_Font_get_characterInfo ();
	Register_UnityEngine_Font_get_characterInfo ();
	void Register_UnityEngine_Font_set_characterInfo ();
	Register_UnityEngine_Font_set_characterInfo ();
	void Register_UnityEngine_Font_RequestCharactersInTexture ();
	Register_UnityEngine_Font_RequestCharactersInTexture ();
	void Register_UnityEngine_Font_GetCharacterInfo ();
	Register_UnityEngine_Font_GetCharacterInfo ();
	void Register_UnityEngine_Font_get_dynamic ();
	Register_UnityEngine_Font_get_dynamic ();
	void Register_UnityEngine_Font_get_ascent ();
	Register_UnityEngine_Font_get_ascent ();
	void Register_UnityEngine_Font_get_lineHeight ();
	Register_UnityEngine_Font_get_lineHeight ();
	void Register_UnityEngine_Font_get_fontSize ();
	Register_UnityEngine_Font_get_fontSize ();
	void Register_UnityEngine_GameObject_CreatePrimitive ();
	Register_UnityEngine_GameObject_CreatePrimitive ();
	void Register_UnityEngine_GameObject_GetComponent ();
	Register_UnityEngine_GameObject_GetComponent ();
	void Register_UnityEngine_GameObject_GetComponentFastPath ();
	Register_UnityEngine_GameObject_GetComponentFastPath ();
	void Register_UnityEngine_GameObject_GetComponentByName ();
	Register_UnityEngine_GameObject_GetComponentByName ();
	void Register_UnityEngine_GameObject_GetComponentInChildren ();
	Register_UnityEngine_GameObject_GetComponentInChildren ();
	void Register_UnityEngine_GameObject_GetComponentInParent ();
	Register_UnityEngine_GameObject_GetComponentInParent ();
	void Register_UnityEngine_GameObject_GetComponentsInternal ();
	Register_UnityEngine_GameObject_GetComponentsInternal ();
	void Register_UnityEngine_GameObject_AddComponentInternal ();
	Register_UnityEngine_GameObject_AddComponentInternal ();
	void Register_UnityEngine_GameObject_get_transform ();
	Register_UnityEngine_GameObject_get_transform ();
	void Register_UnityEngine_GameObject_get_layer ();
	Register_UnityEngine_GameObject_get_layer ();
	void Register_UnityEngine_GameObject_set_layer ();
	Register_UnityEngine_GameObject_set_layer ();
	void Register_UnityEngine_GameObject_get_active ();
	Register_UnityEngine_GameObject_get_active ();
	void Register_UnityEngine_GameObject_set_active ();
	Register_UnityEngine_GameObject_set_active ();
	void Register_UnityEngine_GameObject_SetActive ();
	Register_UnityEngine_GameObject_SetActive ();
	void Register_UnityEngine_GameObject_get_activeSelf ();
	Register_UnityEngine_GameObject_get_activeSelf ();
	void Register_UnityEngine_GameObject_get_activeInHierarchy ();
	Register_UnityEngine_GameObject_get_activeInHierarchy ();
	void Register_UnityEngine_GameObject_SetActiveRecursively ();
	Register_UnityEngine_GameObject_SetActiveRecursively ();
	void Register_UnityEngine_GameObject_get_isStatic ();
	Register_UnityEngine_GameObject_get_isStatic ();
	void Register_UnityEngine_GameObject_set_isStatic ();
	Register_UnityEngine_GameObject_set_isStatic ();
	void Register_UnityEngine_GameObject_get_isStaticBatchable ();
	Register_UnityEngine_GameObject_get_isStaticBatchable ();
	void Register_UnityEngine_GameObject_get_tag ();
	Register_UnityEngine_GameObject_get_tag ();
	void Register_UnityEngine_GameObject_set_tag ();
	Register_UnityEngine_GameObject_set_tag ();
	void Register_UnityEngine_GameObject_CompareTag ();
	Register_UnityEngine_GameObject_CompareTag ();
	void Register_UnityEngine_GameObject_FindGameObjectWithTag ();
	Register_UnityEngine_GameObject_FindGameObjectWithTag ();
	void Register_UnityEngine_GameObject_FindGameObjectsWithTag ();
	Register_UnityEngine_GameObject_FindGameObjectsWithTag ();
	void Register_UnityEngine_GameObject_SendMessageUpwards ();
	Register_UnityEngine_GameObject_SendMessageUpwards ();
	void Register_UnityEngine_GameObject_SendMessage ();
	Register_UnityEngine_GameObject_SendMessage ();
	void Register_UnityEngine_GameObject_BroadcastMessage ();
	Register_UnityEngine_GameObject_BroadcastMessage ();
	void Register_UnityEngine_GameObject_Internal_AddComponentWithType ();
	Register_UnityEngine_GameObject_Internal_AddComponentWithType ();
	void Register_UnityEngine_GameObject_Internal_CreateGameObject ();
	Register_UnityEngine_GameObject_Internal_CreateGameObject ();
	void Register_UnityEngine_GameObject_Find ();
	Register_UnityEngine_GameObject_Find ();
	void Register_UnityEngine_GameObject_INTERNAL_get_scene ();
	Register_UnityEngine_GameObject_INTERNAL_get_scene ();
	void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine ();
	Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine ();
	void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawCube ();
	Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawCube ();
	void Register_UnityEngine_Gizmos_INTERNAL_set_color ();
	Register_UnityEngine_Gizmos_INTERNAL_set_color ();
	void Register_UnityEngine_GL_Vertex3 ();
	Register_UnityEngine_GL_Vertex3 ();
	void Register_UnityEngine_GL_INTERNAL_CALL_Color ();
	Register_UnityEngine_GL_INTERNAL_CALL_Color ();
	void Register_UnityEngine_GL_BeginInternal ();
	Register_UnityEngine_GL_BeginInternal ();
	void Register_UnityEngine_GL_End ();
	Register_UnityEngine_GL_End ();
	void Register_UnityEngine_Gradient_Init ();
	Register_UnityEngine_Gradient_Init ();
	void Register_UnityEngine_Gradient_Cleanup ();
	Register_UnityEngine_Gradient_Cleanup ();
	void Register_UnityEngine_Gradient_INTERNAL_CALL_Evaluate ();
	Register_UnityEngine_Gradient_INTERNAL_CALL_Evaluate ();
	void Register_UnityEngine_Gradient_get_colorKeys ();
	Register_UnityEngine_Gradient_get_colorKeys ();
	void Register_UnityEngine_Gradient_set_colorKeys ();
	Register_UnityEngine_Gradient_set_colorKeys ();
	void Register_UnityEngine_Gradient_get_alphaKeys ();
	Register_UnityEngine_Gradient_get_alphaKeys ();
	void Register_UnityEngine_Gradient_set_alphaKeys ();
	Register_UnityEngine_Gradient_set_alphaKeys ();
	void Register_UnityEngine_Gradient_get_mode ();
	Register_UnityEngine_Gradient_get_mode ();
	void Register_UnityEngine_Gradient_set_mode ();
	Register_UnityEngine_Gradient_set_mode ();
	void Register_UnityEngine_Gradient_SetKeys ();
	Register_UnityEngine_Gradient_SetKeys ();
	void Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount ();
	Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount ();
	void Register_UnityEngine_Graphics_DrawTexture ();
	Register_UnityEngine_Graphics_DrawTexture ();
	void Register_UnityEngine_GUI_INTERNAL_get_color ();
	Register_UnityEngine_GUI_INTERNAL_get_color ();
	void Register_UnityEngine_GUI_INTERNAL_set_color ();
	Register_UnityEngine_GUI_INTERNAL_set_color ();
	void Register_UnityEngine_GUI_INTERNAL_get_backgroundColor ();
	Register_UnityEngine_GUI_INTERNAL_get_backgroundColor ();
	void Register_UnityEngine_GUI_INTERNAL_set_backgroundColor ();
	Register_UnityEngine_GUI_INTERNAL_set_backgroundColor ();
	void Register_UnityEngine_GUI_INTERNAL_get_contentColor ();
	Register_UnityEngine_GUI_INTERNAL_get_contentColor ();
	void Register_UnityEngine_GUI_INTERNAL_set_contentColor ();
	Register_UnityEngine_GUI_INTERNAL_set_contentColor ();
	void Register_UnityEngine_GUI_get_changed ();
	Register_UnityEngine_GUI_get_changed ();
	void Register_UnityEngine_GUI_set_changed ();
	Register_UnityEngine_GUI_set_changed ();
	void Register_UnityEngine_GUI_get_enabled ();
	Register_UnityEngine_GUI_get_enabled ();
	void Register_UnityEngine_GUI_set_enabled ();
	Register_UnityEngine_GUI_set_enabled ();
	void Register_UnityEngine_GUI_Internal_GetTooltip ();
	Register_UnityEngine_GUI_Internal_GetTooltip ();
	void Register_UnityEngine_GUI_Internal_SetTooltip ();
	Register_UnityEngine_GUI_Internal_SetTooltip ();
	void Register_UnityEngine_GUI_Internal_GetMouseTooltip ();
	Register_UnityEngine_GUI_Internal_GetMouseTooltip ();
	void Register_UnityEngine_GUI_get_depth ();
	Register_UnityEngine_GUI_get_depth ();
	void Register_UnityEngine_GUI_set_depth ();
	Register_UnityEngine_GUI_set_depth ();
	void Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel ();
	Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel ();
	void Register_UnityEngine_GUI_InitializeGUIClipTexture ();
	Register_UnityEngine_GUI_InitializeGUIClipTexture ();
	void Register_UnityEngine_GUI_get_blendMaterial ();
	Register_UnityEngine_GUI_get_blendMaterial ();
	void Register_UnityEngine_GUI_get_blitMaterial ();
	Register_UnityEngine_GUI_get_blitMaterial ();
	void Register_UnityEngine_GUI_INTERNAL_CALL_DoButton ();
	Register_UnityEngine_GUI_INTERNAL_CALL_DoButton ();
	void Register_UnityEngine_GUI_SetNextControlName ();
	Register_UnityEngine_GUI_SetNextControlName ();
	void Register_UnityEngine_GUI_GetNameOfFocusedControl ();
	Register_UnityEngine_GUI_GetNameOfFocusedControl ();
	void Register_UnityEngine_GUI_FocusControl ();
	Register_UnityEngine_GUI_FocusControl ();
	void Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle ();
	Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle ();
	void Register_UnityEngine_GUI_get_usePageScrollbars ();
	Register_UnityEngine_GUI_get_usePageScrollbars ();
	void Register_UnityEngine_GUI_InternalRepaintEditorWindow ();
	Register_UnityEngine_GUI_InternalRepaintEditorWindow ();
	void Register_UnityEngine_GUI_INTERNAL_CALL_DoModalWindow ();
	Register_UnityEngine_GUI_INTERNAL_CALL_DoModalWindow ();
	void Register_UnityEngine_GUI_INTERNAL_CALL_DoWindow ();
	Register_UnityEngine_GUI_INTERNAL_CALL_DoWindow ();
	void Register_UnityEngine_GUI_INTERNAL_CALL_DragWindow ();
	Register_UnityEngine_GUI_INTERNAL_CALL_DragWindow ();
	void Register_UnityEngine_GUI_BringWindowToFront ();
	Register_UnityEngine_GUI_BringWindowToFront ();
	void Register_UnityEngine_GUI_BringWindowToBack ();
	Register_UnityEngine_GUI_BringWindowToBack ();
	void Register_UnityEngine_GUI_FocusWindow ();
	Register_UnityEngine_GUI_FocusWindow ();
	void Register_UnityEngine_GUI_UnfocusWindow ();
	Register_UnityEngine_GUI_UnfocusWindow ();
	void Register_UnityEngine_GUI_Internal_BeginWindows ();
	Register_UnityEngine_GUI_Internal_BeginWindows ();
	void Register_UnityEngine_GUI_Internal_EndWindows ();
	Register_UnityEngine_GUI_Internal_EndWindows ();
	void Register_UnityEngine_GUIClip_INTERNAL_CALL_Push ();
	Register_UnityEngine_GUIClip_INTERNAL_CALL_Push ();
	void Register_UnityEngine_GUIClip_Pop ();
	Register_UnityEngine_GUIClip_Pop ();
	void Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2 ();
	Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2 ();
	void Register_UnityEngine_GUIClip_INTERNAL_CALL_Clip_Vector2 ();
	Register_UnityEngine_GUIClip_INTERNAL_CALL_Clip_Vector2 ();
	void Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix ();
	Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix ();
	void Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix ();
	Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix ();
	void Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest ();
	Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest ();
	void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect ();
	Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect ();
	void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow ();
	Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow ();
	void Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed ();
	Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed ();
	void Register_UnityEngine_GUIStyle_Init ();
	Register_UnityEngine_GUIStyle_Init ();
	void Register_UnityEngine_GUIStyle_InitCopy ();
	Register_UnityEngine_GUIStyle_InitCopy ();
	void Register_UnityEngine_GUIStyle_Cleanup ();
	Register_UnityEngine_GUIStyle_Cleanup ();
	void Register_UnityEngine_GUIStyle_get_name ();
	Register_UnityEngine_GUIStyle_get_name ();
	void Register_UnityEngine_GUIStyle_set_name ();
	Register_UnityEngine_GUIStyle_set_name ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetStyleStatePtr ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetStyleStatePtr ();
	void Register_UnityEngine_GUIStyle_AssignStyleState ();
	Register_UnityEngine_GUIStyle_AssignStyleState ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr ();
	void Register_UnityEngine_GUIStyle_AssignRectOffset ();
	Register_UnityEngine_GUIStyle_AssignRectOffset ();
	void Register_UnityEngine_GUIStyle_get_imagePosition ();
	Register_UnityEngine_GUIStyle_get_imagePosition ();
	void Register_UnityEngine_GUIStyle_set_imagePosition ();
	Register_UnityEngine_GUIStyle_set_imagePosition ();
	void Register_UnityEngine_GUIStyle_get_alignment ();
	Register_UnityEngine_GUIStyle_get_alignment ();
	void Register_UnityEngine_GUIStyle_set_alignment ();
	Register_UnityEngine_GUIStyle_set_alignment ();
	void Register_UnityEngine_GUIStyle_get_wordWrap ();
	Register_UnityEngine_GUIStyle_get_wordWrap ();
	void Register_UnityEngine_GUIStyle_set_wordWrap ();
	Register_UnityEngine_GUIStyle_set_wordWrap ();
	void Register_UnityEngine_GUIStyle_get_clipping ();
	Register_UnityEngine_GUIStyle_get_clipping ();
	void Register_UnityEngine_GUIStyle_set_clipping ();
	Register_UnityEngine_GUIStyle_set_clipping ();
	void Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset ();
	Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset ();
	void Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset ();
	Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset ();
	void Register_UnityEngine_GUIStyle_INTERNAL_get_Internal_clipOffset ();
	Register_UnityEngine_GUIStyle_INTERNAL_get_Internal_clipOffset ();
	void Register_UnityEngine_GUIStyle_INTERNAL_set_Internal_clipOffset ();
	Register_UnityEngine_GUIStyle_INTERNAL_set_Internal_clipOffset ();
	void Register_UnityEngine_GUIStyle_get_fixedWidth ();
	Register_UnityEngine_GUIStyle_get_fixedWidth ();
	void Register_UnityEngine_GUIStyle_set_fixedWidth ();
	Register_UnityEngine_GUIStyle_set_fixedWidth ();
	void Register_UnityEngine_GUIStyle_get_fixedHeight ();
	Register_UnityEngine_GUIStyle_get_fixedHeight ();
	void Register_UnityEngine_GUIStyle_set_fixedHeight ();
	Register_UnityEngine_GUIStyle_set_fixedHeight ();
	void Register_UnityEngine_GUIStyle_get_stretchWidth ();
	Register_UnityEngine_GUIStyle_get_stretchWidth ();
	void Register_UnityEngine_GUIStyle_set_stretchWidth ();
	Register_UnityEngine_GUIStyle_set_stretchWidth ();
	void Register_UnityEngine_GUIStyle_get_stretchHeight ();
	Register_UnityEngine_GUIStyle_get_stretchHeight ();
	void Register_UnityEngine_GUIStyle_set_stretchHeight ();
	Register_UnityEngine_GUIStyle_set_stretchHeight ();
	void Register_UnityEngine_GUIStyle_Internal_GetLineHeight ();
	Register_UnityEngine_GUIStyle_Internal_GetLineHeight ();
	void Register_UnityEngine_GUIStyle_SetFontInternal ();
	Register_UnityEngine_GUIStyle_SetFontInternal ();
	void Register_UnityEngine_GUIStyle_GetFontInternalDuringLoadingThread ();
	Register_UnityEngine_GUIStyle_GetFontInternalDuringLoadingThread ();
	void Register_UnityEngine_GUIStyle_GetFontInternal ();
	Register_UnityEngine_GUIStyle_GetFontInternal ();
	void Register_UnityEngine_GUIStyle_get_fontSize ();
	Register_UnityEngine_GUIStyle_get_fontSize ();
	void Register_UnityEngine_GUIStyle_set_fontSize ();
	Register_UnityEngine_GUIStyle_set_fontSize ();
	void Register_UnityEngine_GUIStyle_get_fontStyle ();
	Register_UnityEngine_GUIStyle_get_fontStyle ();
	void Register_UnityEngine_GUIStyle_set_fontStyle ();
	Register_UnityEngine_GUIStyle_set_fontStyle ();
	void Register_UnityEngine_GUIStyle_get_richText ();
	Register_UnityEngine_GUIStyle_get_richText ();
	void Register_UnityEngine_GUIStyle_set_richText ();
	Register_UnityEngine_GUIStyle_set_richText ();
	void Register_UnityEngine_GUIStyle_Internal_Draw ();
	Register_UnityEngine_GUIStyle_Internal_Draw ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2 ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2 ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_SetMouseTooltip ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_SetMouseTooltip ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawPrefixLabel ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawPrefixLabel ();
	void Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset ();
	Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawCursor ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawCursor ();
	void Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection ();
	Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection ();
	void Register_UnityEngine_GUIStyle_SetDefaultFont ();
	Register_UnityEngine_GUIStyle_SetDefaultFont ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex ();
	void Register_UnityEngine_GUIStyle_Internal_GetNumCharactersThatFitWithinWidth ();
	Register_UnityEngine_GUIStyle_Internal_GetNumCharactersThatFitWithinWidth ();
	void Register_UnityEngine_GUIStyle_Internal_CalcSize ();
	Register_UnityEngine_GUIStyle_Internal_CalcSize ();
	void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints ();
	Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints ();
	void Register_UnityEngine_GUIStyle_Internal_CalcHeight ();
	Register_UnityEngine_GUIStyle_Internal_CalcHeight ();
	void Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth ();
	Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth ();
	void Register_UnityEngine_GUIStyleState_Init ();
	Register_UnityEngine_GUIStyleState_Init ();
	void Register_UnityEngine_GUIStyleState_Cleanup ();
	Register_UnityEngine_GUIStyleState_Cleanup ();
	void Register_UnityEngine_GUIStyleState_GetBackgroundInternalFromDeserialization ();
	Register_UnityEngine_GUIStyleState_GetBackgroundInternalFromDeserialization ();
	void Register_UnityEngine_GUIStyleState_GetBackgroundInternal ();
	Register_UnityEngine_GUIStyleState_GetBackgroundInternal ();
	void Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor ();
	Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor ();
	void Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint ();
	Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint ();
	void Register_UnityEngine_GUIUtility_GetControlID ();
	Register_UnityEngine_GUIUtility_GetControlID ();
	void Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2 ();
	Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2 ();
	void Register_UnityEngine_GUIUtility_GetPermanentControlID ();
	Register_UnityEngine_GUIUtility_GetPermanentControlID ();
	void Register_UnityEngine_GUIUtility_Internal_GetHotControl ();
	Register_UnityEngine_GUIUtility_Internal_GetHotControl ();
	void Register_UnityEngine_GUIUtility_Internal_SetHotControl ();
	Register_UnityEngine_GUIUtility_Internal_SetHotControl ();
	void Register_UnityEngine_GUIUtility_UpdateUndoName ();
	Register_UnityEngine_GUIUtility_UpdateUndoName ();
	void Register_UnityEngine_GUIUtility_GrabMouseControl ();
	Register_UnityEngine_GUIUtility_GrabMouseControl ();
	void Register_UnityEngine_GUIUtility_ReleaseMouseControl ();
	Register_UnityEngine_GUIUtility_ReleaseMouseControl ();
	void Register_UnityEngine_GUIUtility_HasMouseControl ();
	Register_UnityEngine_GUIUtility_HasMouseControl ();
	void Register_UnityEngine_GUIUtility_GetChanged ();
	Register_UnityEngine_GUIUtility_GetChanged ();
	void Register_UnityEngine_GUIUtility_SetChanged ();
	Register_UnityEngine_GUIUtility_SetChanged ();
	void Register_UnityEngine_GUIUtility_get_keyboardControl ();
	Register_UnityEngine_GUIUtility_get_keyboardControl ();
	void Register_UnityEngine_GUIUtility_set_keyboardControl ();
	Register_UnityEngine_GUIUtility_set_keyboardControl ();
	void Register_UnityEngine_GUIUtility_SetDidGUIWindowsEatLastEvent ();
	Register_UnityEngine_GUIUtility_SetDidGUIWindowsEatLastEvent ();
	void Register_UnityEngine_GUIUtility_get_systemCopyBuffer ();
	Register_UnityEngine_GUIUtility_get_systemCopyBuffer ();
	void Register_UnityEngine_GUIUtility_set_systemCopyBuffer ();
	Register_UnityEngine_GUIUtility_set_systemCopyBuffer ();
	void Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin ();
	Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin ();
	void Register_UnityEngine_GUIUtility_Internal_GetBuiltinSkin ();
	Register_UnityEngine_GUIUtility_Internal_GetBuiltinSkin ();
	void Register_UnityEngine_GUIUtility_Internal_ExitGUI ();
	Register_UnityEngine_GUIUtility_Internal_ExitGUI ();
	void Register_UnityEngine_GUIUtility_Internal_GetGUIDepth ();
	Register_UnityEngine_GUIUtility_Internal_GetGUIDepth ();
	void Register_UnityEngine_GUIUtility_get_mouseUsed ();
	Register_UnityEngine_GUIUtility_get_mouseUsed ();
	void Register_UnityEngine_GUIUtility_set_mouseUsed ();
	Register_UnityEngine_GUIUtility_set_mouseUsed ();
	void Register_UnityEngine_GUIUtility_get_hasModalWindow ();
	Register_UnityEngine_GUIUtility_get_hasModalWindow ();
	void Register_UnityEngine_GUIUtility_get_textFieldInput ();
	Register_UnityEngine_GUIUtility_get_textFieldInput ();
	void Register_UnityEngine_GUIUtility_set_textFieldInput ();
	Register_UnityEngine_GUIUtility_set_textFieldInput ();
	void Register_UnityEngine_Hash128_Internal_Hash128ToString ();
	Register_UnityEngine_Hash128_Internal_Hash128ToString ();
	void Register_UnityEngine_Input_GetKeyUpInt ();
	Register_UnityEngine_Input_GetKeyUpInt ();
	void Register_UnityEngine_Input_GetKeyDownInt ();
	Register_UnityEngine_Input_GetKeyDownInt ();
	void Register_UnityEngine_Input_GetAxis ();
	Register_UnityEngine_Input_GetAxis ();
	void Register_UnityEngine_Input_GetAxisRaw ();
	Register_UnityEngine_Input_GetAxisRaw ();
	void Register_UnityEngine_Input_GetButton ();
	Register_UnityEngine_Input_GetButton ();
	void Register_UnityEngine_Input_GetButtonDown ();
	Register_UnityEngine_Input_GetButtonDown ();
	void Register_UnityEngine_Input_GetMouseButton ();
	Register_UnityEngine_Input_GetMouseButton ();
	void Register_UnityEngine_Input_GetMouseButtonDown ();
	Register_UnityEngine_Input_GetMouseButtonDown ();
	void Register_UnityEngine_Input_GetMouseButtonUp ();
	Register_UnityEngine_Input_GetMouseButtonUp ();
	void Register_UnityEngine_Input_INTERNAL_get_mousePosition ();
	Register_UnityEngine_Input_INTERNAL_get_mousePosition ();
	void Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta ();
	Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta ();
	void Register_UnityEngine_Input_get_mousePresent ();
	Register_UnityEngine_Input_get_mousePresent ();
	void Register_UnityEngine_Input_INTERNAL_CALL_GetTouch ();
	Register_UnityEngine_Input_INTERNAL_CALL_GetTouch ();
	void Register_UnityEngine_Input_get_touchCount ();
	Register_UnityEngine_Input_get_touchCount ();
	void Register_UnityEngine_Input_get_touchSupported ();
	Register_UnityEngine_Input_get_touchSupported ();
	void Register_UnityEngine_Input_get_imeCompositionMode ();
	Register_UnityEngine_Input_get_imeCompositionMode ();
	void Register_UnityEngine_Input_set_imeCompositionMode ();
	Register_UnityEngine_Input_set_imeCompositionMode ();
	void Register_UnityEngine_Input_get_compositionString ();
	Register_UnityEngine_Input_get_compositionString ();
	void Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos ();
	Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos ();
	void Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos ();
	Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos ();
	void Register_UnityEngine_iOS_LocalNotification_GetFireDate ();
	Register_UnityEngine_iOS_LocalNotification_GetFireDate ();
	void Register_UnityEngine_iOS_LocalNotification_SetFireDate ();
	Register_UnityEngine_iOS_LocalNotification_SetFireDate ();
	void Register_UnityEngine_iOS_LocalNotification_get_timeZone ();
	Register_UnityEngine_iOS_LocalNotification_get_timeZone ();
	void Register_UnityEngine_iOS_LocalNotification_set_timeZone ();
	Register_UnityEngine_iOS_LocalNotification_set_timeZone ();
	void Register_UnityEngine_iOS_LocalNotification_get_repeatInterval ();
	Register_UnityEngine_iOS_LocalNotification_get_repeatInterval ();
	void Register_UnityEngine_iOS_LocalNotification_set_repeatInterval ();
	Register_UnityEngine_iOS_LocalNotification_set_repeatInterval ();
	void Register_UnityEngine_iOS_LocalNotification_get_repeatCalendar ();
	Register_UnityEngine_iOS_LocalNotification_get_repeatCalendar ();
	void Register_UnityEngine_iOS_LocalNotification_set_repeatCalendar ();
	Register_UnityEngine_iOS_LocalNotification_set_repeatCalendar ();
	void Register_UnityEngine_iOS_LocalNotification_get_alertBody ();
	Register_UnityEngine_iOS_LocalNotification_get_alertBody ();
	void Register_UnityEngine_iOS_LocalNotification_set_alertBody ();
	Register_UnityEngine_iOS_LocalNotification_set_alertBody ();
	void Register_UnityEngine_iOS_LocalNotification_get_alertAction ();
	Register_UnityEngine_iOS_LocalNotification_get_alertAction ();
	void Register_UnityEngine_iOS_LocalNotification_set_alertAction ();
	Register_UnityEngine_iOS_LocalNotification_set_alertAction ();
	void Register_UnityEngine_iOS_LocalNotification_get_hasAction ();
	Register_UnityEngine_iOS_LocalNotification_get_hasAction ();
	void Register_UnityEngine_iOS_LocalNotification_set_hasAction ();
	Register_UnityEngine_iOS_LocalNotification_set_hasAction ();
	void Register_UnityEngine_iOS_LocalNotification_get_alertLaunchImage ();
	Register_UnityEngine_iOS_LocalNotification_get_alertLaunchImage ();
	void Register_UnityEngine_iOS_LocalNotification_set_alertLaunchImage ();
	Register_UnityEngine_iOS_LocalNotification_set_alertLaunchImage ();
	void Register_UnityEngine_iOS_LocalNotification_get_applicationIconBadgeNumber ();
	Register_UnityEngine_iOS_LocalNotification_get_applicationIconBadgeNumber ();
	void Register_UnityEngine_iOS_LocalNotification_set_applicationIconBadgeNumber ();
	Register_UnityEngine_iOS_LocalNotification_set_applicationIconBadgeNumber ();
	void Register_UnityEngine_iOS_LocalNotification_get_soundName ();
	Register_UnityEngine_iOS_LocalNotification_get_soundName ();
	void Register_UnityEngine_iOS_LocalNotification_set_soundName ();
	Register_UnityEngine_iOS_LocalNotification_set_soundName ();
	void Register_UnityEngine_iOS_LocalNotification_get_defaultSoundName ();
	Register_UnityEngine_iOS_LocalNotification_get_defaultSoundName ();
	void Register_UnityEngine_iOS_LocalNotification_get_userInfo ();
	Register_UnityEngine_iOS_LocalNotification_get_userInfo ();
	void Register_UnityEngine_iOS_LocalNotification_set_userInfo ();
	Register_UnityEngine_iOS_LocalNotification_set_userInfo ();
	void Register_UnityEngine_iOS_LocalNotification_Destroy ();
	Register_UnityEngine_iOS_LocalNotification_Destroy ();
	void Register_UnityEngine_iOS_LocalNotification_InitWrapper ();
	Register_UnityEngine_iOS_LocalNotification_InitWrapper ();
	void Register_UnityEngine_iOS_RemoteNotification_get_alertBody ();
	Register_UnityEngine_iOS_RemoteNotification_get_alertBody ();
	void Register_UnityEngine_iOS_RemoteNotification_get_hasAction ();
	Register_UnityEngine_iOS_RemoteNotification_get_hasAction ();
	void Register_UnityEngine_iOS_RemoteNotification_get_applicationIconBadgeNumber ();
	Register_UnityEngine_iOS_RemoteNotification_get_applicationIconBadgeNumber ();
	void Register_UnityEngine_iOS_RemoteNotification_get_soundName ();
	Register_UnityEngine_iOS_RemoteNotification_get_soundName ();
	void Register_UnityEngine_iOS_RemoteNotification_get_userInfo ();
	Register_UnityEngine_iOS_RemoteNotification_get_userInfo ();
	void Register_UnityEngine_iOS_RemoteNotification_Destroy ();
	Register_UnityEngine_iOS_RemoteNotification_Destroy ();
	void Register_UnityEngine_Material_INTERNAL_CALL_GetColor ();
	Register_UnityEngine_Material_INTERNAL_CALL_GetColor ();
	void Register_UnityEngine_Material_GetTexture ();
	Register_UnityEngine_Material_GetTexture ();
	void Register_UnityEngine_Material_SetFloat ();
	Register_UnityEngine_Material_SetFloat ();
	void Register_UnityEngine_Material_HasProperty ();
	Register_UnityEngine_Material_HasProperty ();
	void Register_UnityEngine_Material_SetPass ();
	Register_UnityEngine_Material_SetPass ();
	void Register_UnityEngine_Material_Internal_CreateWithShader ();
	Register_UnityEngine_Material_Internal_CreateWithShader ();
	void Register_UnityEngine_Material_Internal_CreateWithMaterial ();
	Register_UnityEngine_Material_Internal_CreateWithMaterial ();
	void Register_UnityEngine_Material_EnableKeyword ();
	Register_UnityEngine_Material_EnableKeyword ();
	void Register_UnityEngine_Material_DisableKeyword ();
	Register_UnityEngine_Material_DisableKeyword ();
	void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS ();
	Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS ();
	void Register_UnityEngine_Mesh_Internal_Create ();
	Register_UnityEngine_Mesh_Internal_Create ();
	void Register_UnityEngine_Mesh_Clear ();
	Register_UnityEngine_Mesh_Clear ();
	void Register_UnityEngine_Mesh_get_isReadable ();
	Register_UnityEngine_Mesh_get_isReadable ();
	void Register_UnityEngine_Mesh_get_canAccess ();
	Register_UnityEngine_Mesh_get_canAccess ();
	void Register_UnityEngine_Mesh_PrintErrorCantAccessMesh ();
	Register_UnityEngine_Mesh_PrintErrorCantAccessMesh ();
	void Register_UnityEngine_Mesh_PrintErrorCantAccessMeshForIndices ();
	Register_UnityEngine_Mesh_PrintErrorCantAccessMeshForIndices ();
	void Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexTriangles ();
	Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexTriangles ();
	void Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexIndices ();
	Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexIndices ();
	void Register_UnityEngine_Mesh_SetArrayForChannelImpl ();
	Register_UnityEngine_Mesh_SetArrayForChannelImpl ();
	void Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl ();
	Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl ();
	void Register_UnityEngine_Mesh_GetArrayFromChannelImpl ();
	Register_UnityEngine_Mesh_GetArrayFromChannelImpl ();
	void Register_UnityEngine_Mesh_HasChannel ();
	Register_UnityEngine_Mesh_HasChannel ();
	void Register_UnityEngine_Mesh_ResizeList ();
	Register_UnityEngine_Mesh_ResizeList ();
	void Register_UnityEngine_Mesh_ExtractArrayFromList ();
	Register_UnityEngine_Mesh_ExtractArrayFromList ();
	void Register_UnityEngine_Mesh_GetTrianglesImpl ();
	Register_UnityEngine_Mesh_GetTrianglesImpl ();
	void Register_UnityEngine_Mesh_GetIndicesImpl ();
	Register_UnityEngine_Mesh_GetIndicesImpl ();
	void Register_UnityEngine_Mesh_SetTrianglesImpl ();
	Register_UnityEngine_Mesh_SetTrianglesImpl ();
	void Register_UnityEngine_Mesh_SetIndicesImpl ();
	Register_UnityEngine_Mesh_SetIndicesImpl ();
	void Register_UnityEngine_Mesh_get_blendShapeCount ();
	Register_UnityEngine_Mesh_get_blendShapeCount ();
	void Register_UnityEngine_Mesh_ClearBlendShapes ();
	Register_UnityEngine_Mesh_ClearBlendShapes ();
	void Register_UnityEngine_Mesh_GetBlendShapeName ();
	Register_UnityEngine_Mesh_GetBlendShapeName ();
	void Register_UnityEngine_Mesh_GetBlendShapeFrameCount ();
	Register_UnityEngine_Mesh_GetBlendShapeFrameCount ();
	void Register_UnityEngine_Mesh_GetBlendShapeFrameWeight ();
	Register_UnityEngine_Mesh_GetBlendShapeFrameWeight ();
	void Register_UnityEngine_Mesh_GetBlendShapeFrameVertices ();
	Register_UnityEngine_Mesh_GetBlendShapeFrameVertices ();
	void Register_UnityEngine_Mesh_AddBlendShapeFrame ();
	Register_UnityEngine_Mesh_AddBlendShapeFrame ();
	void Register_UnityEngine_Mesh_get_vertexBufferCount ();
	Register_UnityEngine_Mesh_get_vertexBufferCount ();
	void Register_UnityEngine_Mesh_INTERNAL_CALL_GetNativeVertexBufferPtr ();
	Register_UnityEngine_Mesh_INTERNAL_CALL_GetNativeVertexBufferPtr ();
	void Register_UnityEngine_Mesh_INTERNAL_CALL_GetNativeIndexBufferPtr ();
	Register_UnityEngine_Mesh_INTERNAL_CALL_GetNativeIndexBufferPtr ();
	void Register_UnityEngine_Mesh_INTERNAL_get_bounds ();
	Register_UnityEngine_Mesh_INTERNAL_get_bounds ();
	void Register_UnityEngine_Mesh_INTERNAL_set_bounds ();
	Register_UnityEngine_Mesh_INTERNAL_set_bounds ();
	void Register_UnityEngine_Mesh_RecalculateBounds ();
	Register_UnityEngine_Mesh_RecalculateBounds ();
	void Register_UnityEngine_Mesh_RecalculateNormals ();
	Register_UnityEngine_Mesh_RecalculateNormals ();
	void Register_UnityEngine_Mesh_Optimize ();
	Register_UnityEngine_Mesh_Optimize ();
	void Register_UnityEngine_Mesh_GetTopology ();
	Register_UnityEngine_Mesh_GetTopology ();
	void Register_UnityEngine_Mesh_get_vertexCount ();
	Register_UnityEngine_Mesh_get_vertexCount ();
	void Register_UnityEngine_Mesh_get_subMeshCount ();
	Register_UnityEngine_Mesh_get_subMeshCount ();
	void Register_UnityEngine_Mesh_set_subMeshCount ();
	Register_UnityEngine_Mesh_set_subMeshCount ();
	void Register_UnityEngine_Mesh_CombineMeshes ();
	Register_UnityEngine_Mesh_CombineMeshes ();
	void Register_UnityEngine_Mesh_get_boneWeights ();
	Register_UnityEngine_Mesh_get_boneWeights ();
	void Register_UnityEngine_Mesh_set_boneWeights ();
	Register_UnityEngine_Mesh_set_boneWeights ();
	void Register_UnityEngine_Mesh_get_bindposes ();
	Register_UnityEngine_Mesh_get_bindposes ();
	void Register_UnityEngine_Mesh_set_bindposes ();
	Register_UnityEngine_Mesh_set_bindposes ();
	void Register_UnityEngine_Mesh_MarkDynamic ();
	Register_UnityEngine_Mesh_MarkDynamic ();
	void Register_UnityEngine_Mesh_UploadMeshData ();
	Register_UnityEngine_Mesh_UploadMeshData ();
	void Register_UnityEngine_Mesh_GetBlendShapeIndex ();
	Register_UnityEngine_Mesh_GetBlendShapeIndex ();
	void Register_UnityEngine_MeshRenderer_get_additionalVertexStreams ();
	Register_UnityEngine_MeshRenderer_get_additionalVertexStreams ();
	void Register_UnityEngine_MeshRenderer_set_additionalVertexStreams ();
	Register_UnityEngine_MeshRenderer_set_additionalVertexStreams ();
	void Register_UnityEngine_MonoBehaviour__ctor ();
	Register_UnityEngine_MonoBehaviour__ctor ();
	void Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll ();
	Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll ();
	void Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll ();
	Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll ();
	void Register_UnityEngine_MonoBehaviour_Invoke ();
	Register_UnityEngine_MonoBehaviour_Invoke ();
	void Register_UnityEngine_MonoBehaviour_InvokeRepeating ();
	Register_UnityEngine_MonoBehaviour_InvokeRepeating ();
	void Register_UnityEngine_MonoBehaviour_CancelInvoke ();
	Register_UnityEngine_MonoBehaviour_CancelInvoke ();
	void Register_UnityEngine_MonoBehaviour_IsInvoking ();
	Register_UnityEngine_MonoBehaviour_IsInvoking ();
	void Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal ();
	Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal ();
	void Register_UnityEngine_MonoBehaviour_StartCoroutine ();
	Register_UnityEngine_MonoBehaviour_StartCoroutine ();
	void Register_UnityEngine_MonoBehaviour_StopCoroutine ();
	Register_UnityEngine_MonoBehaviour_StopCoroutine ();
	void Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto ();
	Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto ();
	void Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto ();
	Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto ();
	void Register_UnityEngine_MonoBehaviour_StopAllCoroutines ();
	Register_UnityEngine_MonoBehaviour_StopAllCoroutines ();
	void Register_UnityEngine_MonoBehaviour_get_useGUILayout ();
	Register_UnityEngine_MonoBehaviour_get_useGUILayout ();
	void Register_UnityEngine_MonoBehaviour_set_useGUILayout ();
	Register_UnityEngine_MonoBehaviour_set_useGUILayout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitWrapper ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitWrapper ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_AddChannel ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_AddChannel ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitPacketSize ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitPacketSize ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitFragmentSize ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitFragmentSize ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitResendTimeout ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitResendTimeout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitDisconnectTimeout ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitDisconnectTimeout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitConnectTimeout ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitConnectTimeout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMinUpdateTimeout ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitMinUpdateTimeout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitPingTimeout ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitPingTimeout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitReducedPingTimeout ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitReducedPingTimeout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitAllCostTimeout ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitAllCostTimeout ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitNetworkDropThreshold ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitNetworkDropThreshold ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitOverflowDropThreshold ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitOverflowDropThreshold ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxConnectionAttempt ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxConnectionAttempt ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitAckDelay ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitAckDelay ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageSize ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageSize ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageCount ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageCount ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxSentMessageQueueSize ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxSentMessageQueueSize ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitIsAcksLong ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitIsAcksLong ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitUsePlatformSpecificProtocols ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitUsePlatformSpecificProtocols ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_InitWebSocketReceiveBufferMaxSize ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_InitWebSocketReceiveBufferMaxSize ();
	void Register_UnityEngine_Networking_ConnectionConfigInternal_Dispose ();
	Register_UnityEngine_Networking_ConnectionConfigInternal_Dispose ();
	void Register_UnityEngine_Networking_ConnectionSimulatorConfig__ctor ();
	Register_UnityEngine_Networking_ConnectionSimulatorConfig__ctor ();
	void Register_UnityEngine_Networking_ConnectionSimulatorConfig_Dispose ();
	Register_UnityEngine_Networking_ConnectionSimulatorConfig_Dispose ();
	void Register_UnityEngine_Networking_DownloadHandler_InternalCreateBuffer ();
	Register_UnityEngine_Networking_DownloadHandler_InternalCreateBuffer ();
	void Register_UnityEngine_Networking_DownloadHandler_InternalCreateScript ();
	Register_UnityEngine_Networking_DownloadHandler_InternalCreateScript ();
	void Register_UnityEngine_Networking_DownloadHandler_InternalCreateTexture ();
	Register_UnityEngine_Networking_DownloadHandler_InternalCreateTexture ();
	void Register_UnityEngine_Networking_DownloadHandler_InternalCreateAssetBundle ();
	Register_UnityEngine_Networking_DownloadHandler_InternalCreateAssetBundle ();
	void Register_UnityEngine_Networking_DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle ();
	Register_UnityEngine_Networking_DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle ();
	void Register_UnityEngine_Networking_DownloadHandler_InternalCreateAudioClip ();
	Register_UnityEngine_Networking_DownloadHandler_InternalCreateAudioClip ();
	void Register_UnityEngine_Networking_DownloadHandler_InternalDestroy ();
	Register_UnityEngine_Networking_DownloadHandler_InternalDestroy ();
	void Register_UnityEngine_Networking_DownloadHandler_get_isDone ();
	Register_UnityEngine_Networking_DownloadHandler_get_isDone ();
	void Register_UnityEngine_Networking_GlobalConfigInternal_InitWrapper ();
	Register_UnityEngine_Networking_GlobalConfigInternal_InitWrapper ();
	void Register_UnityEngine_Networking_GlobalConfigInternal_InitThreadAwakeTimeout ();
	Register_UnityEngine_Networking_GlobalConfigInternal_InitThreadAwakeTimeout ();
	void Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorModel ();
	Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorModel ();
	void Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumReceivedMessages ();
	Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumReceivedMessages ();
	void Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumSentMessages ();
	Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumSentMessages ();
	void Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxPacketSize ();
	Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxPacketSize ();
	void Register_UnityEngine_Networking_GlobalConfigInternal_Dispose ();
	Register_UnityEngine_Networking_GlobalConfigInternal_Dispose ();
	void Register_UnityEngine_Networking_HostTopologyInternal_InitWrapper ();
	Register_UnityEngine_Networking_HostTopologyInternal_InitWrapper ();
	void Register_UnityEngine_Networking_HostTopologyInternal_AddSpecialConnectionConfigWrapper ();
	Register_UnityEngine_Networking_HostTopologyInternal_AddSpecialConnectionConfigWrapper ();
	void Register_UnityEngine_Networking_HostTopologyInternal_InitReceivedPoolSize ();
	Register_UnityEngine_Networking_HostTopologyInternal_InitReceivedPoolSize ();
	void Register_UnityEngine_Networking_HostTopologyInternal_InitSentMessagePoolSize ();
	Register_UnityEngine_Networking_HostTopologyInternal_InitSentMessagePoolSize ();
	void Register_UnityEngine_Networking_HostTopologyInternal_InitMessagePoolSizeGrowthFactor ();
	Register_UnityEngine_Networking_HostTopologyInternal_InitMessagePoolSizeGrowthFactor ();
	void Register_UnityEngine_Networking_HostTopologyInternal_Dispose ();
	Register_UnityEngine_Networking_HostTopologyInternal_Dispose ();
	void Register_UnityEngine_Networking_NetworkTransport_InitWithNoParameters ();
	Register_UnityEngine_Networking_NetworkTransport_InitWithNoParameters ();
	void Register_UnityEngine_Networking_NetworkTransport_InitWithParameters ();
	Register_UnityEngine_Networking_NetworkTransport_InitWithParameters ();
	void Register_UnityEngine_Networking_NetworkTransport_ConnectAsNetworkHost ();
	Register_UnityEngine_Networking_NetworkTransport_ConnectAsNetworkHost ();
	void Register_UnityEngine_Networking_NetworkTransport_ReceiveRelayEventFromHost ();
	Register_UnityEngine_Networking_NetworkTransport_ReceiveRelayEventFromHost ();
	void Register_UnityEngine_Networking_NetworkTransport_ConnectToNetworkPeer ();
	Register_UnityEngine_Networking_NetworkTransport_ConnectToNetworkPeer ();
	void Register_UnityEngine_Networking_NetworkTransport_GetConnectionInfo ();
	Register_UnityEngine_Networking_NetworkTransport_GetConnectionInfo ();
	void Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapper ();
	Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapper ();
	void Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapperWithoutIp ();
	Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapperWithoutIp ();
	void Register_UnityEngine_Networking_NetworkTransport_AddHostWrapper ();
	Register_UnityEngine_Networking_NetworkTransport_AddHostWrapper ();
	void Register_UnityEngine_Networking_NetworkTransport_AddHostWrapperWithoutIp ();
	Register_UnityEngine_Networking_NetworkTransport_AddHostWrapperWithoutIp ();
	void Register_UnityEngine_Networking_NetworkTransport_RemoveHost ();
	Register_UnityEngine_Networking_NetworkTransport_RemoveHost ();
	void Register_UnityEngine_Networking_NetworkTransport_get_IsStarted ();
	Register_UnityEngine_Networking_NetworkTransport_get_IsStarted ();
	void Register_UnityEngine_Networking_NetworkTransport_Connect ();
	Register_UnityEngine_Networking_NetworkTransport_Connect ();
	void Register_UnityEngine_Networking_NetworkTransport_Internal_ConnectEndPoint ();
	Register_UnityEngine_Networking_NetworkTransport_Internal_ConnectEndPoint ();
	void Register_UnityEngine_Networking_NetworkTransport_ConnectWithSimulator ();
	Register_UnityEngine_Networking_NetworkTransport_ConnectWithSimulator ();
	void Register_UnityEngine_Networking_NetworkTransport_Disconnect ();
	Register_UnityEngine_Networking_NetworkTransport_Disconnect ();
	void Register_UnityEngine_Networking_NetworkTransport_SendWrapper ();
	Register_UnityEngine_Networking_NetworkTransport_SendWrapper ();
	void Register_UnityEngine_Networking_NetworkTransport_ReceiveFromHost ();
	Register_UnityEngine_Networking_NetworkTransport_ReceiveFromHost ();
	void Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithoutData ();
	Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithoutData ();
	void Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithData ();
	Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithData ();
	void Register_UnityEngine_Networking_NetworkTransport_StopBroadcastDiscovery ();
	Register_UnityEngine_Networking_NetworkTransport_StopBroadcastDiscovery ();
	void Register_UnityEngine_Networking_NetworkTransport_SetBroadcastCredentials ();
	Register_UnityEngine_Networking_NetworkTransport_SetBroadcastCredentials ();
	void Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionInfo ();
	Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionInfo ();
	void Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionMessage ();
	Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionMessage ();
	void Register_UnityEngine_Networking_UnityWebRequest_InternalDestroy ();
	Register_UnityEngine_Networking_UnityWebRequest_InternalDestroy ();
	void Register_UnityEngine_Networking_UnityWebRequest_get_error ();
	Register_UnityEngine_Networking_UnityWebRequest_get_error ();
	void Register_UnityEngine_Networking_UnityWebRequest_get_isDone ();
	Register_UnityEngine_Networking_UnityWebRequest_get_isDone ();
	void Register_UnityEngine_Networking_UnityWebRequest_get_isError ();
	Register_UnityEngine_Networking_UnityWebRequest_get_isError ();
	void Register_UnityEngine_Networking_UnityWebRequest_get_uploadHandler ();
	Register_UnityEngine_Networking_UnityWebRequest_get_uploadHandler ();
	void Register_UnityEngine_Networking_UnityWebRequest_get_downloadHandler ();
	Register_UnityEngine_Networking_UnityWebRequest_get_downloadHandler ();
	void Register_UnityEngine_Networking_UploadHandler_InternalDestroy ();
	Register_UnityEngine_Networking_UploadHandler_InternalDestroy ();
	void Register_UnityEngine_NetworkMessageInfo_NullNetworkView ();
	Register_UnityEngine_NetworkMessageInfo_NullNetworkView ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetIPAddress ();
	Register_UnityEngine_NetworkPlayer_Internal_GetIPAddress ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetPort ();
	Register_UnityEngine_NetworkPlayer_Internal_GetPort ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetExternalIP ();
	Register_UnityEngine_NetworkPlayer_Internal_GetExternalIP ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetExternalPort ();
	Register_UnityEngine_NetworkPlayer_Internal_GetExternalPort ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetLocalIP ();
	Register_UnityEngine_NetworkPlayer_Internal_GetLocalIP ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetLocalPort ();
	Register_UnityEngine_NetworkPlayer_Internal_GetLocalPort ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetPlayerIndex ();
	Register_UnityEngine_NetworkPlayer_Internal_GetPlayerIndex ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetGUID ();
	Register_UnityEngine_NetworkPlayer_Internal_GetGUID ();
	void Register_UnityEngine_NetworkPlayer_Internal_GetLocalGUID ();
	Register_UnityEngine_NetworkPlayer_Internal_GetLocalGUID ();
	void Register_UnityEngine_NetworkView_INTERNAL_CALL_Find ();
	Register_UnityEngine_NetworkView_INTERNAL_CALL_Find ();
	void Register_UnityEngine_NetworkViewID_INTERNAL_get_unassigned ();
	Register_UnityEngine_NetworkViewID_INTERNAL_get_unassigned ();
	void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_IsMine ();
	Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_IsMine ();
	void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetOwner ();
	Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetOwner ();
	void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetString ();
	Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetString ();
	void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_Compare ();
	Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_Compare ();
	void Register_UnityEngine_Object_Internal_CloneSingle ();
	Register_UnityEngine_Object_Internal_CloneSingle ();
	void Register_UnityEngine_Object_Internal_CloneSingleWithParent ();
	Register_UnityEngine_Object_Internal_CloneSingleWithParent ();
	void Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle ();
	Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle ();
	void Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent ();
	Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent ();
	void Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject ();
	Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject ();
	void Register_UnityEngine_Object_EnsureRunningOnMainThread ();
	Register_UnityEngine_Object_EnsureRunningOnMainThread ();
	void Register_UnityEngine_Object_Destroy ();
	Register_UnityEngine_Object_Destroy ();
	void Register_UnityEngine_Object_DestroyImmediate ();
	Register_UnityEngine_Object_DestroyImmediate ();
	void Register_UnityEngine_Object_FindObjectsOfType ();
	Register_UnityEngine_Object_FindObjectsOfType ();
	void Register_UnityEngine_Object_get_name ();
	Register_UnityEngine_Object_get_name ();
	void Register_UnityEngine_Object_set_name ();
	Register_UnityEngine_Object_set_name ();
	void Register_UnityEngine_Object_DontDestroyOnLoad ();
	Register_UnityEngine_Object_DontDestroyOnLoad ();
	void Register_UnityEngine_Object_get_hideFlags ();
	Register_UnityEngine_Object_get_hideFlags ();
	void Register_UnityEngine_Object_set_hideFlags ();
	Register_UnityEngine_Object_set_hideFlags ();
	void Register_UnityEngine_Object_DestroyObject ();
	Register_UnityEngine_Object_DestroyObject ();
	void Register_UnityEngine_Object_FindSceneObjectsOfType ();
	Register_UnityEngine_Object_FindSceneObjectsOfType ();
	void Register_UnityEngine_Object_FindObjectsOfTypeIncludingAssets ();
	Register_UnityEngine_Object_FindObjectsOfTypeIncludingAssets ();
	void Register_UnityEngine_Object_ToString ();
	Register_UnityEngine_Object_ToString ();
	void Register_UnityEngine_Object_DoesObjectWithInstanceIDExist ();
	Register_UnityEngine_Object_DoesObjectWithInstanceIDExist ();
	void Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll ();
	Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll ();
	void Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere ();
	Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere ();
	void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast ();
	Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast ();
	void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest ();
	Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest ();
	void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast ();
	Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast ();
	void Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll ();
	Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll ();
	void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircle ();
	Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircle ();
	void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleAll ();
	Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleAll ();
	void Register_UnityEngine_Ping__ctor ();
	Register_UnityEngine_Ping__ctor ();
	void Register_UnityEngine_Ping_DestroyPing ();
	Register_UnityEngine_Ping_DestroyPing ();
	void Register_UnityEngine_Ping_get_isDone ();
	Register_UnityEngine_Ping_get_isDone ();
	void Register_UnityEngine_Ping_get_time ();
	Register_UnityEngine_Ping_get_time ();
	void Register_UnityEngine_Ping_get_ip ();
	Register_UnityEngine_Ping_get_ip ();
	void Register_UnityEngine_PlayerPrefs_TrySetInt ();
	Register_UnityEngine_PlayerPrefs_TrySetInt ();
	void Register_UnityEngine_PlayerPrefs_GetInt ();
	Register_UnityEngine_PlayerPrefs_GetInt ();
	void Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis ();
	Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis ();
	void Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation ();
	Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation ();
	void Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation ();
	Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation ();
	void Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp ();
	Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp ();
	void Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse ();
	Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse ();
	void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad ();
	Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad ();
	void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad ();
	Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad ();
	void Register_UnityEngine_Random_Range ();
	Register_UnityEngine_Random_Range ();
	void Register_UnityEngine_Random_RandomRangeInt ();
	Register_UnityEngine_Random_RandomRangeInt ();
	void Register_UnityEngine_RectOffset_Init ();
	Register_UnityEngine_RectOffset_Init ();
	void Register_UnityEngine_RectOffset_Cleanup ();
	Register_UnityEngine_RectOffset_Cleanup ();
	void Register_UnityEngine_RectOffset_get_left ();
	Register_UnityEngine_RectOffset_get_left ();
	void Register_UnityEngine_RectOffset_set_left ();
	Register_UnityEngine_RectOffset_set_left ();
	void Register_UnityEngine_RectOffset_get_right ();
	Register_UnityEngine_RectOffset_get_right ();
	void Register_UnityEngine_RectOffset_set_right ();
	Register_UnityEngine_RectOffset_set_right ();
	void Register_UnityEngine_RectOffset_get_top ();
	Register_UnityEngine_RectOffset_get_top ();
	void Register_UnityEngine_RectOffset_set_top ();
	Register_UnityEngine_RectOffset_set_top ();
	void Register_UnityEngine_RectOffset_get_bottom ();
	Register_UnityEngine_RectOffset_get_bottom ();
	void Register_UnityEngine_RectOffset_set_bottom ();
	Register_UnityEngine_RectOffset_set_bottom ();
	void Register_UnityEngine_RectOffset_get_horizontal ();
	Register_UnityEngine_RectOffset_get_horizontal ();
	void Register_UnityEngine_RectOffset_get_vertical ();
	Register_UnityEngine_RectOffset_get_vertical ();
	void Register_UnityEngine_RectOffset_INTERNAL_CALL_Add ();
	Register_UnityEngine_RectOffset_INTERNAL_CALL_Add ();
	void Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove ();
	Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove ();
	void Register_UnityEngine_RectTransform_INTERNAL_get_rect ();
	Register_UnityEngine_RectTransform_INTERNAL_get_rect ();
	void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin ();
	Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin ();
	void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin ();
	Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin ();
	void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax ();
	Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax ();
	void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax ();
	Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax ();
	void Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition ();
	Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition ();
	void Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition ();
	Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition ();
	void Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta ();
	Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta ();
	void Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta ();
	Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta ();
	void Register_UnityEngine_RectTransform_INTERNAL_get_pivot ();
	Register_UnityEngine_RectTransform_INTERNAL_get_pivot ();
	void Register_UnityEngine_RectTransform_INTERNAL_set_pivot ();
	Register_UnityEngine_RectTransform_INTERNAL_set_pivot ();
	void Register_UnityEngine_RectTransform_get_drivenByObject ();
	Register_UnityEngine_RectTransform_get_drivenByObject ();
	void Register_UnityEngine_RectTransform_set_drivenByObject ();
	Register_UnityEngine_RectTransform_set_drivenByObject ();
	void Register_UnityEngine_RectTransform_get_drivenProperties ();
	Register_UnityEngine_RectTransform_get_drivenProperties ();
	void Register_UnityEngine_RectTransform_set_drivenProperties ();
	Register_UnityEngine_RectTransform_set_drivenProperties ();
	void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint ();
	Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint ();
	void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint ();
	Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint ();
	void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect ();
	Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect ();
	void Register_UnityEngine_Renderer_set_enabled ();
	Register_UnityEngine_Renderer_set_enabled ();
	void Register_UnityEngine_Renderer_INTERNAL_get_bounds ();
	Register_UnityEngine_Renderer_INTERNAL_get_bounds ();
	void Register_UnityEngine_Renderer_get_sortingLayerID ();
	Register_UnityEngine_Renderer_get_sortingLayerID ();
	void Register_UnityEngine_Renderer_get_sortingOrder ();
	Register_UnityEngine_Renderer_get_sortingOrder ();
	void Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer ();
	Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer ();
	void Register_UnityEngine_RenderTexture_Internal_GetWidth ();
	Register_UnityEngine_RenderTexture_Internal_GetWidth ();
	void Register_UnityEngine_RenderTexture_Internal_GetHeight ();
	Register_UnityEngine_RenderTexture_Internal_GetHeight ();
	void Register_UnityEngine_Resources_FindObjectsOfTypeAll ();
	Register_UnityEngine_Resources_FindObjectsOfTypeAll ();
	void Register_UnityEngine_Resources_Load ();
	Register_UnityEngine_Resources_Load ();
	void Register_UnityEngine_Resources_LoadAsync ();
	Register_UnityEngine_Resources_LoadAsync ();
	void Register_UnityEngine_Resources_LoadAll ();
	Register_UnityEngine_Resources_LoadAll ();
	void Register_UnityEngine_Resources_GetBuiltinResource ();
	Register_UnityEngine_Resources_GetBuiltinResource ();
	void Register_UnityEngine_Resources_UnloadAsset ();
	Register_UnityEngine_Resources_UnloadAsset ();
	void Register_UnityEngine_Resources_UnloadUnusedAssets ();
	Register_UnityEngine_Resources_UnloadUnusedAssets ();
	void Register_UnityEngine_Rigidbody_INTERNAL_get_velocity ();
	Register_UnityEngine_Rigidbody_INTERNAL_get_velocity ();
	void Register_UnityEngine_Rigidbody_INTERNAL_set_velocity ();
	Register_UnityEngine_Rigidbody_INTERNAL_set_velocity ();
	void Register_UnityEngine_Rigidbody_INTERNAL_get_angularVelocity ();
	Register_UnityEngine_Rigidbody_INTERNAL_get_angularVelocity ();
	void Register_UnityEngine_Rigidbody_INTERNAL_set_angularVelocity ();
	Register_UnityEngine_Rigidbody_INTERNAL_set_angularVelocity ();
	void Register_UnityEngine_Rigidbody_INTERNAL_get_position ();
	Register_UnityEngine_Rigidbody_INTERNAL_get_position ();
	void Register_UnityEngine_Rigidbody_INTERNAL_set_position ();
	Register_UnityEngine_Rigidbody_INTERNAL_set_position ();
	void Register_UnityEngine_Rigidbody_INTERNAL_get_rotation ();
	Register_UnityEngine_Rigidbody_INTERNAL_get_rotation ();
	void Register_UnityEngine_Rigidbody_INTERNAL_set_rotation ();
	Register_UnityEngine_Rigidbody_INTERNAL_set_rotation ();
	void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition ();
	Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition ();
	void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation ();
	Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation ();
	void Register_UnityEngine_Rigidbody2D_INTERNAL_get_position ();
	Register_UnityEngine_Rigidbody2D_INTERNAL_get_position ();
	void Register_UnityEngine_Rigidbody2D_INTERNAL_set_position ();
	Register_UnityEngine_Rigidbody2D_INTERNAL_set_position ();
	void Register_UnityEngine_Rigidbody2D_get_rotation ();
	Register_UnityEngine_Rigidbody2D_get_rotation ();
	void Register_UnityEngine_Rigidbody2D_set_rotation ();
	Register_UnityEngine_Rigidbody2D_set_rotation ();
	void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_MoveRotation ();
	Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_MoveRotation ();
	void Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity ();
	Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity ();
	void Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity ();
	Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity ();
	void Register_UnityEngine_Rigidbody2D_get_angularVelocity ();
	Register_UnityEngine_Rigidbody2D_get_angularVelocity ();
	void Register_UnityEngine_Rigidbody2D_set_angularVelocity ();
	Register_UnityEngine_Rigidbody2D_set_angularVelocity ();
	void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce ();
	Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce ();
	void Register_UnityEngine_SceneManagement_Scene_GetNameInternal ();
	Register_UnityEngine_SceneManagement_Scene_GetNameInternal ();
	void Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal ();
	Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal ();
	void Register_UnityEngine_SceneManagement_SceneManager_get_sceneCountInBuildSettings ();
	Register_UnityEngine_SceneManagement_SceneManager_get_sceneCountInBuildSettings ();
	void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene ();
	Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene ();
	void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneAt ();
	Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneAt ();
	void Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal ();
	Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal ();
	void Register_UnityEngine_SceneManagement_SceneManager_UnloadSceneNameIndexInternal ();
	Register_UnityEngine_SceneManagement_SceneManager_UnloadSceneNameIndexInternal ();
	void Register_UnityEngine_Screen_get_width ();
	Register_UnityEngine_Screen_get_width ();
	void Register_UnityEngine_Screen_get_height ();
	Register_UnityEngine_Screen_get_height ();
	void Register_UnityEngine_Screen_get_dpi ();
	Register_UnityEngine_Screen_get_dpi ();
	void Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject ();
	Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject ();
	void Register_UnityEngine_ScriptableObject_INTERNAL_CALL_SetDirty ();
	Register_UnityEngine_ScriptableObject_INTERNAL_CALL_SetDirty ();
	void Register_UnityEngine_ScriptableObject_CreateInstance ();
	Register_UnityEngine_ScriptableObject_CreateInstance ();
	void Register_UnityEngine_ScriptableObject_CreateInstanceFromType ();
	Register_UnityEngine_ScriptableObject_CreateInstanceFromType ();
	void Register_UnityEngine_Shader_Find ();
	Register_UnityEngine_Shader_Find ();
	void Register_UnityEngine_Shader_PropertyToID ();
	Register_UnityEngine_Shader_PropertyToID ();
	void Register_UnityEngine_SkinnedMeshRenderer_get_bones ();
	Register_UnityEngine_SkinnedMeshRenderer_get_bones ();
	void Register_UnityEngine_SkinnedMeshRenderer_set_bones ();
	Register_UnityEngine_SkinnedMeshRenderer_set_bones ();
	void Register_UnityEngine_SkinnedMeshRenderer_get_rootBone ();
	Register_UnityEngine_SkinnedMeshRenderer_get_rootBone ();
	void Register_UnityEngine_SkinnedMeshRenderer_set_rootBone ();
	Register_UnityEngine_SkinnedMeshRenderer_set_rootBone ();
	void Register_UnityEngine_SkinnedMeshRenderer_get_quality ();
	Register_UnityEngine_SkinnedMeshRenderer_get_quality ();
	void Register_UnityEngine_SkinnedMeshRenderer_set_quality ();
	Register_UnityEngine_SkinnedMeshRenderer_set_quality ();
	void Register_UnityEngine_SkinnedMeshRenderer_get_sharedMesh ();
	Register_UnityEngine_SkinnedMeshRenderer_get_sharedMesh ();
	void Register_UnityEngine_SkinnedMeshRenderer_set_sharedMesh ();
	Register_UnityEngine_SkinnedMeshRenderer_set_sharedMesh ();
	void Register_UnityEngine_SkinnedMeshRenderer_get_updateWhenOffscreen ();
	Register_UnityEngine_SkinnedMeshRenderer_get_updateWhenOffscreen ();
	void Register_UnityEngine_SkinnedMeshRenderer_set_updateWhenOffscreen ();
	Register_UnityEngine_SkinnedMeshRenderer_set_updateWhenOffscreen ();
	void Register_UnityEngine_SkinnedMeshRenderer_get_skinnedMotionVectors ();
	Register_UnityEngine_SkinnedMeshRenderer_get_skinnedMotionVectors ();
	void Register_UnityEngine_SkinnedMeshRenderer_set_skinnedMotionVectors ();
	Register_UnityEngine_SkinnedMeshRenderer_set_skinnedMotionVectors ();
	void Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_get_localBounds ();
	Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_get_localBounds ();
	void Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_set_localBounds ();
	Register_UnityEngine_SkinnedMeshRenderer_INTERNAL_set_localBounds ();
	void Register_UnityEngine_SkinnedMeshRenderer_BakeMesh ();
	Register_UnityEngine_SkinnedMeshRenderer_BakeMesh ();
	void Register_UnityEngine_SkinnedMeshRenderer_GetBlendShapeWeight ();
	Register_UnityEngine_SkinnedMeshRenderer_GetBlendShapeWeight ();
	void Register_UnityEngine_SkinnedMeshRenderer_SetBlendShapeWeight ();
	Register_UnityEngine_SkinnedMeshRenderer_SetBlendShapeWeight ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading ();
	void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose ();
	Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose ();
	void Register_UnityEngine_SortingLayer_GetLayerValueFromID ();
	Register_UnityEngine_SortingLayer_GetLayerValueFromID ();
	void Register_UnityEngine_Sprite_INTERNAL_get_rect ();
	Register_UnityEngine_Sprite_INTERNAL_get_rect ();
	void Register_UnityEngine_Sprite_get_pixelsPerUnit ();
	Register_UnityEngine_Sprite_get_pixelsPerUnit ();
	void Register_UnityEngine_Sprite_get_texture ();
	Register_UnityEngine_Sprite_get_texture ();
	void Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture ();
	Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture ();
	void Register_UnityEngine_Sprite_INTERNAL_get_textureRect ();
	Register_UnityEngine_Sprite_INTERNAL_get_textureRect ();
	void Register_UnityEngine_Sprite_get_packed ();
	Register_UnityEngine_Sprite_get_packed ();
	void Register_UnityEngine_Sprite_INTERNAL_get_border ();
	Register_UnityEngine_Sprite_INTERNAL_get_border ();
	void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV ();
	Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV ();
	void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV ();
	Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV ();
	void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding ();
	Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding ();
	void Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize ();
	Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize ();
	void Register_UnityEngine_SystemInfo_get_operatingSystemFamily ();
	Register_UnityEngine_SystemInfo_get_operatingSystemFamily ();
	void Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier ();
	Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier ();
	void Register_UnityEngine_TextAsset_get_text ();
	Register_UnityEngine_TextAsset_get_text ();
	void Register_UnityEngine_TextGenerator_Init ();
	Register_UnityEngine_TextGenerator_Init ();
	void Register_UnityEngine_TextGenerator_Dispose_cpp ();
	Register_UnityEngine_TextGenerator_Dispose_cpp ();
	void Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp ();
	Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp ();
	void Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents ();
	Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents ();
	void Register_UnityEngine_TextGenerator_get_vertexCount ();
	Register_UnityEngine_TextGenerator_get_vertexCount ();
	void Register_UnityEngine_TextGenerator_GetVerticesInternal ();
	Register_UnityEngine_TextGenerator_GetVerticesInternal ();
	void Register_UnityEngine_TextGenerator_GetVerticesArray ();
	Register_UnityEngine_TextGenerator_GetVerticesArray ();
	void Register_UnityEngine_TextGenerator_get_characterCount ();
	Register_UnityEngine_TextGenerator_get_characterCount ();
	void Register_UnityEngine_TextGenerator_GetCharactersInternal ();
	Register_UnityEngine_TextGenerator_GetCharactersInternal ();
	void Register_UnityEngine_TextGenerator_GetCharactersArray ();
	Register_UnityEngine_TextGenerator_GetCharactersArray ();
	void Register_UnityEngine_TextGenerator_get_lineCount ();
	Register_UnityEngine_TextGenerator_get_lineCount ();
	void Register_UnityEngine_TextGenerator_GetLinesInternal ();
	Register_UnityEngine_TextGenerator_GetLinesInternal ();
	void Register_UnityEngine_TextGenerator_GetLinesArray ();
	Register_UnityEngine_TextGenerator_GetLinesArray ();
	void Register_UnityEngine_TextGenerator_get_fontSizeUsedForBestFit ();
	Register_UnityEngine_TextGenerator_get_fontSizeUsedForBestFit ();
	void Register_UnityEngine_Texture_Internal_GetWidth ();
	Register_UnityEngine_Texture_Internal_GetWidth ();
	void Register_UnityEngine_Texture_Internal_GetHeight ();
	Register_UnityEngine_Texture_Internal_GetHeight ();
	void Register_UnityEngine_Texture_get_wrapMode ();
	Register_UnityEngine_Texture_get_wrapMode ();
	void Register_UnityEngine_Texture_INTERNAL_get_texelSize ();
	Register_UnityEngine_Texture_INTERNAL_get_texelSize ();
	void Register_UnityEngine_Texture2D_Internal_Create ();
	Register_UnityEngine_Texture2D_Internal_Create ();
	void Register_UnityEngine_Texture2D_get_whiteTexture ();
	Register_UnityEngine_Texture2D_get_whiteTexture ();
	void Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear ();
	Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear ();
	void Register_UnityEngine_Time_get_time ();
	Register_UnityEngine_Time_get_time ();
	void Register_UnityEngine_Time_get_deltaTime ();
	Register_UnityEngine_Time_get_deltaTime ();
	void Register_UnityEngine_Time_get_unscaledTime ();
	Register_UnityEngine_Time_get_unscaledTime ();
	void Register_UnityEngine_Time_get_unscaledDeltaTime ();
	Register_UnityEngine_Time_get_unscaledDeltaTime ();
	void Register_UnityEngine_Time_get_fixedDeltaTime ();
	Register_UnityEngine_Time_get_fixedDeltaTime ();
	void Register_UnityEngine_Time_get_smoothDeltaTime ();
	Register_UnityEngine_Time_get_smoothDeltaTime ();
	void Register_UnityEngine_Time_get_timeScale ();
	Register_UnityEngine_Time_get_timeScale ();
	void Register_UnityEngine_Time_set_timeScale ();
	Register_UnityEngine_Time_set_timeScale ();
	void Register_UnityEngine_Time_get_frameCount ();
	Register_UnityEngine_Time_get_frameCount ();
	void Register_UnityEngine_Time_get_realtimeSinceStartup ();
	Register_UnityEngine_Time_get_realtimeSinceStartup ();
	void Register_UnityEngine_TouchScreenKeyboard_Destroy ();
	Register_UnityEngine_TouchScreenKeyboard_Destroy ();
	void Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper ();
	Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper ();
	void Register_UnityEngine_TouchScreenKeyboard_get_text ();
	Register_UnityEngine_TouchScreenKeyboard_get_text ();
	void Register_UnityEngine_TouchScreenKeyboard_set_text ();
	Register_UnityEngine_TouchScreenKeyboard_set_text ();
	void Register_UnityEngine_TouchScreenKeyboard_set_hideInput ();
	Register_UnityEngine_TouchScreenKeyboard_set_hideInput ();
	void Register_UnityEngine_TouchScreenKeyboard_get_active ();
	Register_UnityEngine_TouchScreenKeyboard_get_active ();
	void Register_UnityEngine_TouchScreenKeyboard_set_active ();
	Register_UnityEngine_TouchScreenKeyboard_set_active ();
	void Register_UnityEngine_TouchScreenKeyboard_get_done ();
	Register_UnityEngine_TouchScreenKeyboard_get_done ();
	void Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled ();
	Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled ();
	void Register_UnityEngine_Transform_INTERNAL_get_position ();
	Register_UnityEngine_Transform_INTERNAL_get_position ();
	void Register_UnityEngine_Transform_INTERNAL_set_position ();
	Register_UnityEngine_Transform_INTERNAL_set_position ();
	void Register_UnityEngine_Transform_INTERNAL_get_localPosition ();
	Register_UnityEngine_Transform_INTERNAL_get_localPosition ();
	void Register_UnityEngine_Transform_INTERNAL_set_localPosition ();
	Register_UnityEngine_Transform_INTERNAL_set_localPosition ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_GetLocalEulerAngles ();
	Register_UnityEngine_Transform_INTERNAL_CALL_GetLocalEulerAngles ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_SetLocalEulerAngles ();
	Register_UnityEngine_Transform_INTERNAL_CALL_SetLocalEulerAngles ();
	void Register_UnityEngine_Transform_INTERNAL_get_rotation ();
	Register_UnityEngine_Transform_INTERNAL_get_rotation ();
	void Register_UnityEngine_Transform_INTERNAL_set_rotation ();
	Register_UnityEngine_Transform_INTERNAL_set_rotation ();
	void Register_UnityEngine_Transform_INTERNAL_get_localRotation ();
	Register_UnityEngine_Transform_INTERNAL_get_localRotation ();
	void Register_UnityEngine_Transform_INTERNAL_set_localRotation ();
	Register_UnityEngine_Transform_INTERNAL_set_localRotation ();
	void Register_UnityEngine_Transform_INTERNAL_get_localScale ();
	Register_UnityEngine_Transform_INTERNAL_get_localScale ();
	void Register_UnityEngine_Transform_INTERNAL_set_localScale ();
	Register_UnityEngine_Transform_INTERNAL_set_localScale ();
	void Register_UnityEngine_Transform_get_parentInternal ();
	Register_UnityEngine_Transform_get_parentInternal ();
	void Register_UnityEngine_Transform_set_parentInternal ();
	Register_UnityEngine_Transform_set_parentInternal ();
	void Register_UnityEngine_Transform_SetParent ();
	Register_UnityEngine_Transform_SetParent ();
	void Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix ();
	Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix ();
	void Register_UnityEngine_Transform_INTERNAL_get_localToWorldMatrix ();
	Register_UnityEngine_Transform_INTERNAL_get_localToWorldMatrix ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundInternal ();
	Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundInternal ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_LookAt ();
	Register_UnityEngine_Transform_INTERNAL_CALL_LookAt ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection ();
	Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection ();
	Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_TransformVector ();
	Register_UnityEngine_Transform_INTERNAL_CALL_TransformVector ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformVector ();
	Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformVector ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint ();
	Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint ();
	Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint ();
	void Register_UnityEngine_Transform_get_root ();
	Register_UnityEngine_Transform_get_root ();
	void Register_UnityEngine_Transform_get_childCount ();
	Register_UnityEngine_Transform_get_childCount ();
	void Register_UnityEngine_Transform_DetachChildren ();
	Register_UnityEngine_Transform_DetachChildren ();
	void Register_UnityEngine_Transform_SetAsFirstSibling ();
	Register_UnityEngine_Transform_SetAsFirstSibling ();
	void Register_UnityEngine_Transform_SetAsLastSibling ();
	Register_UnityEngine_Transform_SetAsLastSibling ();
	void Register_UnityEngine_Transform_SetSiblingIndex ();
	Register_UnityEngine_Transform_SetSiblingIndex ();
	void Register_UnityEngine_Transform_GetSiblingIndex ();
	Register_UnityEngine_Transform_GetSiblingIndex ();
	void Register_UnityEngine_Transform_Find ();
	Register_UnityEngine_Transform_Find ();
	void Register_UnityEngine_Transform_INTERNAL_get_lossyScale ();
	Register_UnityEngine_Transform_INTERNAL_get_lossyScale ();
	void Register_UnityEngine_Transform_IsChildOf ();
	Register_UnityEngine_Transform_IsChildOf ();
	void Register_UnityEngine_Transform_get_hasChanged ();
	Register_UnityEngine_Transform_get_hasChanged ();
	void Register_UnityEngine_Transform_set_hasChanged ();
	Register_UnityEngine_Transform_set_hasChanged ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_RotateAround ();
	Register_UnityEngine_Transform_INTERNAL_CALL_RotateAround ();
	void Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundLocal ();
	Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundLocal ();
	void Register_UnityEngine_Transform_GetChild ();
	Register_UnityEngine_Transform_GetChild ();
	void Register_UnityEngine_Transform_GetChildCount ();
	Register_UnityEngine_Transform_GetChildCount ();
	void Register_UnityEngine_Transform_get_hierarchyCapacity ();
	Register_UnityEngine_Transform_get_hierarchyCapacity ();
	void Register_UnityEngine_Transform_set_hierarchyCapacity ();
	Register_UnityEngine_Transform_set_hierarchyCapacity ();
	void Register_UnityEngine_Transform_get_hierarchyCount ();
	Register_UnityEngine_Transform_get_hierarchyCount ();
	void Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler ();
	Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler ();
	void Register_UnityEngine_UnityLogWriter_WriteStringToUnityLog ();
	Register_UnityEngine_UnityLogWriter_WriteStringToUnityLog ();
	void Register_UnityEngine_WWW_DestroyWWW ();
	Register_UnityEngine_WWW_DestroyWWW ();
	void Register_UnityEngine_WWW_InitWWW ();
	Register_UnityEngine_WWW_InitWWW ();
	void Register_UnityEngine_WWW_get_responseHeadersString ();
	Register_UnityEngine_WWW_get_responseHeadersString ();
	void Register_UnityEngine_WWW_get_bytes ();
	Register_UnityEngine_WWW_get_bytes ();
	void Register_UnityEngine_WWW_get_error ();
	Register_UnityEngine_WWW_get_error ();
	void Register_UnityEngine_WWW_get_isDone ();
	Register_UnityEngine_WWW_get_isDone ();
}

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_UNET();
	RegisterModule_UNET();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

}

template <typename T> void RegisterClass();
template <typename T> void RegisterStrippedTypeInfo(int, const char*, const char*);

namespace  { class EditorExtension; }

class Component;

namespace  { class Behaviour; }

namespace  { class Animation; }

namespace  { class AudioBehaviour; }

namespace  { class AudioListener; }

namespace  { class AudioSource; }

namespace  { class AudioFilter; }

namespace  { class AudioChorusFilter; }

namespace  { class AudioDistortionFilter; }

namespace  { class AudioEchoFilter; }

namespace  { class AudioHighPassFilter; }

namespace  { class AudioLowPassFilter; }

namespace  { class AudioReverbFilter; }

namespace  { class AudioReverbZone; }

namespace  { class Camera; }

class Canvas;

class CanvasGroup;

class Cloth;

namespace  { class Collider2D; }

namespace  { class BoxCollider2D; }

namespace  { class CapsuleCollider2D; }

namespace  { class CircleCollider2D; }

namespace  { class EdgeCollider2D; }

namespace  { class PolygonCollider2D; }

namespace  { class ConstantForce; }

namespace  { class DirectorPlayer; }

namespace  { class Animator; }

namespace  { class Effector2D; }

namespace  { class AreaEffector2D; }

namespace  { class BuoyancyEffector2D; }

namespace  { class PlatformEffector2D; }

namespace  { class PointEffector2D; }

namespace  { class SurfaceEffector2D; }

namespace  { class FlareLayer; }

namespace  { class GUIElement; }

class GUIText;

namespace  { class GUITexture; }

namespace  { class GUILayer; }

namespace  { class Halo; }

namespace  { class HaloLayer; }

namespace  { class Joint2D; }

namespace  { class AnchoredJoint2D; }

namespace  { class DistanceJoint2D; }

namespace  { class FixedJoint2D; }

namespace  { class FrictionJoint2D; }

namespace  { class HingeJoint2D; }

namespace  { class SliderJoint2D; }

namespace  { class SpringJoint2D; }

namespace  { class WheelJoint2D; }

namespace  { class RelativeJoint2D; }

namespace  { class TargetJoint2D; }

namespace  { class LensFlare; }

namespace  { class Light; }

namespace  { class LightProbeGroup; }

namespace  { class LightProbeProxyVolume; }

namespace  { class MonoBehaviour; }

namespace  { class NavMeshAgent; }

namespace  { class NavMeshObstacle; }

namespace  { class NetworkView; }

namespace  { class OffMeshLink; }

namespace  { class PhysicsUpdateBehaviour2D; }

namespace  { class ConstantForce2D; }

namespace  { class Projector; }

namespace  { class ReflectionProbe; }

namespace  { class Skybox; }

namespace  { class Terrain; }

namespace  { class WindZone; }

class CanvasRenderer;

namespace  { class Collider; }

namespace  { class BoxCollider; }

namespace  { class CapsuleCollider; }

namespace  { class CharacterController; }

namespace  { class MeshCollider; }

namespace  { class SphereCollider; }

namespace  { class TerrainCollider; }

namespace  { class WheelCollider; }

class Joint;

class CharacterJoint;

class ConfigurableJoint;

class FixedJoint;

class HingeJoint;

class SpringJoint;

namespace  { class LODGroup; }

namespace  { class MeshFilter; }

namespace  { class OcclusionArea; }

namespace  { class OcclusionPortal; }

namespace  { class ParticleAnimator; }

namespace  { class ParticleEmitter; }

namespace  { class EllipsoidParticleEmitter; }

namespace  { class MeshParticleEmitter; }

namespace  { class ParticleSystem; }

namespace  { class Renderer; }

namespace  { class BillboardRenderer; }

namespace  { class LineRenderer; }

namespace  { class MeshRenderer; }

namespace  { class ParticleRenderer; }

namespace  { class ParticleSystemRenderer; }

namespace  { class SkinnedMeshRenderer; }

namespace  { class SpriteRenderer; }

namespace  { class TrailRenderer; }

namespace  { class Rigidbody; }

namespace  { class Rigidbody2D; }

class TextMesh;

namespace  { class Transform; }

class RectTransform;

namespace  { class Tree; }

namespace  { class WorldAnchor; }

namespace  { class WorldParticleCollider; }

namespace  { class GameObject; }

namespace  { class NamedObject; }

namespace  { class AssetBundle; }

namespace  { class AssetBundleManifest; }

namespace  { class AudioMixer; }

namespace  { class AudioMixerController; }

namespace  { class AudioMixerGroup; }

namespace  { class AudioMixerGroupController; }

namespace  { class AudioMixerSnapshot; }

namespace  { class AudioMixerSnapshotController; }

namespace  { class Avatar; }

namespace  { class BillboardAsset; }

namespace  { class ComputeShader; }

namespace  { class Flare; }

class Font;

namespace  { class LightProbes; }

namespace  { class Material; }

namespace  { class ProceduralMaterial; }

namespace  { class Mesh; }

namespace  { class Motion; }

namespace  { class AnimationClip; }

namespace  { class NavMeshData; }

namespace  { class OcclusionCullingData; }

namespace  { class PhysicMaterial; }

namespace  { class PhysicsMaterial2D; }

namespace  { class PreloadData; }

namespace  { class RuntimeAnimatorController; }

namespace  { class AnimatorController; }

namespace  { class AnimatorOverrideController; }

namespace  { class SampleClip; }

namespace  { class AudioClip; }

namespace  { class Shader; }

namespace  { class ShaderVariantCollection; }

namespace  { class SpeedTreeWindAsset; }

namespace  { class Sprite; }

namespace  { class SubstanceArchive; }

namespace  { class TerrainData; }

namespace  { class TextAsset; }

namespace  { class CGProgram; }

namespace  { class MonoScript; }

namespace  { class Texture; }

namespace  { class BaseVideoTexture; }

namespace  { class MovieTexture; }

namespace  { class CubemapArray; }

namespace  { class ProceduralTexture; }

namespace  { class RenderTexture; }

namespace  { class SparseTexture; }

namespace  { class Texture2D; }

namespace  { class Cubemap; }

namespace  { class Texture2DArray; }

namespace  { class Texture3D; }

namespace  { class WebCamTexture; }

namespace  { class GameManager; }

namespace  { class GlobalGameManager; }

namespace  { class AudioManager; }

namespace  { class BuildSettings; }

namespace  { class CloudWebServicesManager; }

namespace  { class ClusterInputManager; }

namespace  { class CrashReportManager; }

namespace  { class DelayedCallManager; }

namespace  { class GraphicsSettings; }

namespace  { class InputManager; }

namespace  { class MasterServerInterface; }

namespace  { class MonoManager; }

namespace  { class NavMeshProjectSettings; }

namespace  { class NetworkManager; }

namespace  { class Physics2DSettings; }

namespace  { class PhysicsManager; }

namespace  { class PlayerSettings; }

namespace  { class QualitySettings; }

namespace  { class ResourceManager; }

namespace  { class RuntimeInitializeOnLoadManager; }

namespace  { class ScriptMapper; }

namespace  { class TagManager; }

namespace  { class TimeManager; }

namespace  { class UnityAdsManager; }

namespace  { class UnityAnalyticsManager; }

namespace  { class UnityConnectSettings; }

namespace  { class LevelGameManager; }

namespace  { class LightmapSettings; }

namespace  { class NavMeshSettings; }

namespace  { class OcclusionCullingSettings; }

namespace  { class RenderSettings; }

namespace  { class NScreenBridge; }

void RegisterAllClasses() 
{
	void RegisterBuiltinTypes();
	RegisterBuiltinTypes();
	// Non stripped classes
	RegisterClass<EditorExtension>();
	RegisterClass<Unity::Component>();
	RegisterClass<Behaviour>();
	RegisterClass<AudioBehaviour>();
	RegisterClass<AudioListener>();
	RegisterClass<AudioSource>();
	RegisterClass<Camera>();
	RegisterClass<UI::Canvas>();
	RegisterClass<Collider2D>();
	RegisterClass<BoxCollider2D>();
	RegisterClass<CircleCollider2D>();
	RegisterClass<DirectorPlayer>();
	RegisterClass<Animator>();
	RegisterClass<GUILayer>();
	RegisterClass<MonoBehaviour>();
	RegisterClass<UI::CanvasRenderer>();
	RegisterClass<Renderer>();
	RegisterClass<SpriteRenderer>();
	RegisterClass<Rigidbody2D>();
	RegisterClass<Transform>();
	RegisterClass<UI::RectTransform>();
	RegisterClass<GameObject>();
	RegisterClass<NamedObject>();
	RegisterClass<TextRendering::Font>();
	RegisterClass<Material>();
	RegisterClass<Mesh>();
	RegisterClass<Motion>();
	RegisterClass<AnimationClip>();
	RegisterClass<PhysicsMaterial2D>();
	RegisterClass<PreloadData>();
	RegisterClass<RuntimeAnimatorController>();
	RegisterClass<AnimatorController>();
	RegisterClass<SampleClip>();
	RegisterClass<AudioClip>();
	RegisterClass<Shader>();
	RegisterClass<Sprite>();
	RegisterClass<TextAsset>();
	RegisterClass<MonoScript>();
	RegisterClass<Texture>();
	RegisterClass<RenderTexture>();
	RegisterClass<Texture2D>();
	RegisterClass<Cubemap>();
	RegisterClass<Texture2DArray>();
	RegisterClass<Texture3D>();
	RegisterClass<GameManager>();
	RegisterClass<GlobalGameManager>();
	RegisterClass<AudioManager>();
	RegisterClass<BuildSettings>();
	RegisterClass<DelayedCallManager>();
	RegisterClass<GraphicsSettings>();
	RegisterClass<InputManager>();
	RegisterClass<MasterServerInterface>();
	RegisterClass<MonoManager>();
	RegisterClass<NetworkManager>();
	RegisterClass<Physics2DSettings>();
	RegisterClass<PhysicsManager>();
	RegisterClass<PlayerSettings>();
	RegisterClass<QualitySettings>();
	RegisterClass<ResourceManager>();
	RegisterClass<RuntimeInitializeOnLoadManager>();
	RegisterClass<ScriptMapper>();
	RegisterClass<TagManager>();
	RegisterClass<TimeManager>();
	RegisterClass<LevelGameManager>();
	RegisterClass<LightmapSettings>();
	RegisterClass<RenderSettings>();


}
